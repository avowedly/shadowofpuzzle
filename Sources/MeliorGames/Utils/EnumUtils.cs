using System;

namespace MeliorGames.Utils
{
	public sealed class EnumUtils
	{
		public static TEnum Parse<TEnum>(string value)
		{
			return (TEnum) Parse (value, typeof(TEnum));
		}

		public static object Parse(string value, Type enumType)
		{
			if (!enumType.IsEnum) 
			{
				throw new ArgumentException(string.Format("Provided type '{0}' must be an Enum", enumType), "T");
			}
			
			int numValue;
			if ( int.TryParse(value, out numValue) )
			{
				return Enum.ToObject(enumType, numValue);
			}
			else if ( Enum.IsDefined(enumType, value) )
			{
				return Enum.Parse(enumType, value, true);
			}
			
			throw new Exception(string.Format("Unable to parse enum of type '{0}' from value '{1}'", enumType, value));
		}

		public static T? ParseSafe<T>(string enumString)
			where T : struct
		{
			Type enumType = typeof(T);

			object value = ParseSafe (enumString, enumType);

			return value != null ? (T)value : default(T?);
		}

		public static object ParseSafe(string enumString, Type enumType)
		{
			if (Enum.IsDefined(enumType, enumString))
			{
				try
				{
					return Enum.Parse(enumType, enumString, true);
				}
				catch
				{
				}
			}
			
			return null;
		}
	}
}