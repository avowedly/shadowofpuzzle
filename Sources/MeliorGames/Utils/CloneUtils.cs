using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Reflection;
using System.Collections.Generic;

namespace MeliorGames.Utils
{
	public static class CloneUtils
	{
		public static TObject Clone<TObject>(TObject obj)
		{
			return GenericCopier<TObject>.DeepCopy(obj);
		}

		public static object Clone(object obj)
		{
			return GenericCopier<object>.DeepCopy(obj);
		}

		public static void ApplyAvailableFields(object target, object source)
		{
			Dictionary<string, FieldInfo> sourceFieldsMap = GetFieldsMap(source);
			Dictionary<string, FieldInfo> targetFieldsMap = GetFieldsMap(target);

			foreach (string key in sourceFieldsMap.Keys)
			{
				if (!targetFieldsMap.ContainsKey(key))
					continue;

				FieldInfo sourceField = sourceFieldsMap[key];
				FieldInfo targetField = targetFieldsMap[key];

				targetField.SetValue(target, sourceField.GetValue(source));
			}
		}

		private static Dictionary<string, FieldInfo> GetFieldsMap(object source)
		{
			Dictionary<string, FieldInfo> result = new Dictionary<string, FieldInfo>();

			foreach(FieldInfo field in source.GetType().GetFields())
			{
				result.Add(field.Name+"_"+field.FieldType, field);
			}

			return result;
		}
	}

	internal static class GenericCopier<T>
	{
		public static T DeepCopy(object objectToCopy)
		{
			using (MemoryStream memoryStream = new MemoryStream())
			{
				BinaryFormatter binaryFormatter = new BinaryFormatter();
				binaryFormatter.Serialize(memoryStream, objectToCopy);
				memoryStream.Seek(0, SeekOrigin.Begin);
				return (T) binaryFormatter.Deserialize(memoryStream);
			}
		}
	}

	internal static class SwapElements<T>
	{
		public static void Swap( ref T first, ref T second )
		{
			T tmp = first;
			first = second;
			second = tmp;
		}
	}
}