// 
//  DataException.cs
//  
//  Author:
//       Denis Oleynik <denis@meliorgames.com>
// 
//  Copyright (c) 2013 Melior Games Inc.
// 
//  
using System;

namespace ShadowPuzzles.Data
{
	public sealed class DataException:Exception
	{
		public DataException (String message):base(message)
		{
		}
		
		public DataException (String message, Exception innerException):base(message,innerException)
		{
		}
	}
}

