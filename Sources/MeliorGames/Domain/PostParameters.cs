
namespace MeliorGames.Domain
{
	public sealed class PostParameters
	{
		public readonly string Message;
		public readonly Photo Photo;
		public readonly string Subject;
		public readonly string ToAdress;
		public readonly string PhotoPath;

		public readonly string Link;
		public readonly bool IsHtml;
				
		public PostParameters(string message, Photo photo = null)
		{
			this.Message = message;
			this.Photo = photo;
			this.Subject = string.Empty;
			this.ToAdress = string.Empty;
			this.IsHtml = true;
		}
		
		public PostParameters(string subject, string message, Photo photo = null)
		{
			this.Message = message;
			this.Photo = photo;
			this.Subject = subject;
			this.ToAdress = string.Empty;

			this.IsHtml = true;
		}	

		public PostParameters(string subject, string message, string toAdress, Photo photo = null)
		{
			this.Message = message;
			this.Photo = photo;
			this.Subject = subject;
			this.ToAdress = toAdress;

			this.IsHtml = true;
		}	

		public PostParameters(string subject, string message, string toAdress, bool isHtml)
		{
			this.Message = message;
			this.Subject = subject;
			this.ToAdress = toAdress;
			this.Photo = null;

			this.IsHtml = isHtml;
		}	

		public PostParameters(string subject, string message, string photoPath, string link)
		{
			this.Message = message;
			this.Subject = subject;
			this.Link = link;
			this.PhotoPath = photoPath;

			this.IsHtml = true;
		}	
	}
}

