// 
//  DomainObject.cs
//  
//  Author:
//       Denis Oleynik <denis@meliorgames.com>
// 
//  Copyright (c) 2013 Melior Games Inc.
// 
//  
using System;
using MeliorGames.Core;
using System.Xml.Serialization;
using System.Runtime.Serialization;

namespace MeliorGames.Domain
{
	public abstract class DomainObject:Observable
	{
		[XmlAttribute]
		public string Id;
		
		public DomainObject ()
		{
			this.Id = Guid.NewGuid().ToString();			
		}

		public DomainObject(SerializationInfo info, StreamingContext context):base(info,context)
		{
		}
	}
}