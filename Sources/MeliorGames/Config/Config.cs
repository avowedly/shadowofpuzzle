// 
//  Config.cs
//  
//  Author:
//       Denis Oleynik <denis@meliorgames.com>
// 
//  Copyright (c) 2013 Melior Games Inc.
// 
//  
using System;
using MeliorGames.Data.Serialization;
using UnityEngine;
using System.IO;
using MeliorGames.Core;

namespace MeliorGames.Data
{
    public enum StorageType
    {
        Documents,
        StreamingAssets
    }

	public static class ConfigHelper
	{
		public const string CONFIGS_FOLDER = "Configs";
		#if UNITY_EDITOR
		private static readonly string _applicationDataPath = Application.dataPath;
		#endif
		private static string _documentsPath;

		public static string GetPath(StorageType storage)
		{
			string folder = string.Empty;
			
			if(storage == StorageType.Documents)
			{
				folder = GetDocumentsPath();
				
				if(!Directory.Exists(folder))
					Directory.CreateDirectory(folder);
			}
			else
			{
				folder = Application.streamingAssetsPath;
				folder = Path.Combine(folder,CONFIGS_FOLDER);
			}
			
			return folder;
		}
		
		private static string GetDocumentsPath()
		{
			if (!string.IsNullOrEmpty(_documentsPath))
				return _documentsPath;
			
			#if UNITY_EDITOR
			_documentsPath = System.IO.Path.Combine(_applicationDataPath,"..");
			_documentsPath = System.IO.Path.Combine(_documentsPath,"Data");
			#else
			_documentsPath = Application.persistentDataPath;
			#endif
			
			return _documentsPath;
		}
	}

	public abstract class Config<TConfig,TSerializationPolicy> :Observable
		where TSerializationPolicy:SerializationPolicy,new()
		where TConfig:Config<TConfig,TSerializationPolicy>
	{
		private string _name;

        private string _path;

		public string ResultPath
		{
			get
			{
				return _path;
			}
		}

		public Config (String name)
		{
			_name = name;
		}
		
		public virtual void OnRestored()
		{
		}

		public static string GetPath(StorageType storage)
		{
			return ConfigHelper.GetPath(storage);
		}

        public static TConfig Load(StorageType storage, string name) 
        {
			return Load(Path.Combine(GetPath(storage), GetNameWithExtension(name)));
        }

		public static TConfig Load(StorageType storage, string name, MemoryStream stream) 
		{
			TSerializationPolicy serializer = new TSerializationPolicy();
			
			TConfig config = null;
			
			using(stream)
			{
				config = serializer.Restore<TConfig>(stream);
			}

			config._path = Path.Combine(GetPath(storage), GetNameWithExtension(name));
			config.OnRestored();
			
			return config;
		}

		private static TConfig Load(string filepath) 
		{
			TSerializationPolicy serializer = new TSerializationPolicy();
			
			TConfig config = null;

			using(Stream stream = File.OpenRead(filepath))
			{
				config = serializer.Restore<TConfig>(stream);
			}

			config._path = filepath;
			config.OnRestored();
			
			return config;
		}

		private static string GetNameWithExtension(string name)
		{
			TSerializationPolicy serializer = new TSerializationPolicy();
			return string.Format("{0}.{1}",name, serializer.FileExtention);
		}

		public static bool IsExists (StorageType storage, string name)
		{
			return File.Exists(Path.Combine(GetPath(storage), GetNameWithExtension(name)));
		}

		public void Save(StorageType storage)
		{
			Save(storage, _name);
		}

		public void Save(StorageType storage, string filename)
		{
			string filenameWithExt = GetNameWithExtension(filename);

			if(string.IsNullOrEmpty(_path))
			{
				_path = GetPath( storage );
				
				if(!Directory.Exists(_path))
					Directory.CreateDirectory(_path);
				
				_path = Path.Combine(_path,filenameWithExt);
			}

			TSerializationPolicy serializer = new TSerializationPolicy();

//			MonoLog.Log(MonoLogChannel.Configs, "Saving config " + _path);
			
			using(Stream stream = File.Open(_path, FileMode.Create, FileAccess.Write))
			{
				serializer.Store<TConfig>((TConfig)this,stream);
			}
		}
	}
}