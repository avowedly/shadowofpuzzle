using System;
using System.Collections.Generic;

namespace MeliorGames.Core.Commands
{
	public sealed class StateCommandExecutionDescription
	{
		public readonly StateMachine StateMachine;
		public readonly Type CommandType;
		public readonly object[] Arguments;
		
		public StateCommandExecutionDescription (StateMachine stateMachine, Type commandType, object[] arguments)
		{
			this.StateMachine = stateMachine;
			this.CommandType = commandType;
			this.Arguments = arguments;
		}
	}

	public abstract class ExecuteSetCommand : StateCommand
	{
		public int FaultsCount
		{
			get; protected set;
		}

		protected override void OnStart (object[] args)
		{
			base.OnStart (args);

			this.FaultsCount = 0;
		}

		protected void OnFinish()
		{
			FinishCommand( FaultsCount == 0 );
		}
	}
}