using System.Collections.Generic;

namespace MeliorGames.Core.Commands
{
	public sealed class ExecuteSequenceCommand : ExecuteSetCommand
	{
		private Queue<StateCommandExecutionDescription> _executionDescriptions;
		
		protected override void OnStart (object[] args)
		{
			base.OnStart (args);
			
			_executionDescriptions = (Queue<StateCommandExecutionDescription>) args[0];
			
			ExecuteNext();
		}

		private void ExecuteNext()
		{
			if (_executionDescriptions.Count > 0)
			{
				StateCommandExecutionDescription commandDescription = _executionDescriptions.Dequeue();

				commandDescription.StateMachine.Execute(commandDescription.CommandType, commandDescription.Arguments).AsyncToken.AddResponder( new Responder<StateCommand>(OnSuccess, OnFault) );
			}
			else
			{
				OnFinish();
			}
		}
		
		private void OnSuccess(StateCommand command)
		{
			OnResult(command, true);
		}
		
		private void OnFault(StateCommand command)
		{
			OnResult(command, false);
		}
		
		private void OnResult(StateCommand command, bool success)
		{
			if (IsRunning)
			{
				if (!success)
				{
					FaultsCount++;
				}
				
				ExecuteNext();
			}
		}
	}
}