using System;
using System.Collections.Generic;

namespace MeliorGames.Core.Commands
{
	public sealed class ExecuteGroupCommand : ExecuteSetCommand
	{
		private List<StateCommandExecutionDescription> _executionDescriptions;

		private List<StateCommand> _executingCommands;

		protected override void OnStart (object[] args)
		{
			base.OnStart (args);

			_executionDescriptions = (List<StateCommandExecutionDescription>) args[0];
			
			_executingCommands = new List<StateCommand>();

			foreach (StateCommandExecutionDescription commandDescription in _executionDescriptions)
			{
				StateCommand executingCommand = commandDescription.StateMachine.Execute(commandDescription.CommandType, commandDescription.Arguments);
				executingCommand.AsyncToken.AddResponder( new Responder<StateCommand>(OnSuccess, OnFault) );
				_executingCommands.Add(executingCommand);
			}
			
			if (_executingCommands.Count == 0)
			{
				OnFinish();
			}
		}

		private void OnSuccess(StateCommand command)
		{
			OnResult(command, true);
		}

		private void OnFault(StateCommand command)
		{
			OnResult(command, false);
		}

		private void OnResult(StateCommand command, bool success)
		{
			if (IsRunning)
			{
				_executingCommands.Remove(command);

				if (!success)
				{
					FaultsCount++;
				}
				
				if (_executingCommands.Count == 0)
				{
					OnFinish();
				}
			}
		}
	}
}