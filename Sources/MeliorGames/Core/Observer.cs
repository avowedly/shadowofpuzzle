// 
//  Observer.cs
//  
//  Author:
//       Denis Oleynik <denis@meliorgames.com>
// 
//  Copyright (c) 2013 Melior Games Inc.
// 
//  
using System;

namespace MeliorGames.Core
{
	public interface Observer
	{
		 void OnObjectChanged(Observable observable);
	}
}

