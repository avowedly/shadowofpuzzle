// 
//  MonoScheduledBehaviour.cs
//  
//  Author:
//       Denis Oleynik <denis@meliorgames.com>
// 
//  Copyright (c) 2013 Melior Games Inc.
// 
//  
using System;
using UnityEngine;
using MeliorGames.UI;

namespace MeliorGames.Core
{
	public abstract class MonoScheduledBehaviour:MGMonoBehaviour
	{
		private float _scheduleUpdateInterval;
		private float _scheduleUpdateTime;
		
		[SerializeField]
		private bool _scheduledUpdate;
		private bool _scheduleRepeat;	

		private bool _scheduleChangeRequest;
		
		public MonoScheduledBehaviour ()
		{
		}
		
		protected void UnscheduleUpdate()
		{
			_scheduledUpdate = false;			
		}
		
		protected void ScheduleUpdate(float interval, bool repeat = true)
		{
			_scheduledUpdate = true;
			_scheduleUpdateInterval = interval;
			_scheduleRepeat = repeat;
			_scheduleChangeRequest = true;
		}

		protected virtual void Update()
		{
			if (_scheduleChangeRequest) 
			{
				_scheduleChangeRequest = false;

				_scheduleUpdateTime = Time.time + _scheduleUpdateInterval;
			}

			if(_scheduledUpdate && Time.time > _scheduleUpdateTime)
			{
				if(_scheduleRepeat)
					_scheduleUpdateTime = Time.time + _scheduleUpdateInterval;
				else
					_scheduledUpdate = false;
				
				OnScheduledUpdate();						
			}	
		}
		
		protected virtual void OnScheduledUpdate()
		{
		}	
	}

	abstract public class MGScheduledComponentWithModel<T>:MonoScheduledBehaviour,IObserver where T : Observable
	{
		private Boolean _isStarted = false;
		private Boolean _isActivationWithModel;
		private T _model;
		
		public T Model
		{
			get
			{
				return _model;
			}
			set
			{
				if (null != _model)
				{
					_model.RemoveObserver(this);
				}
				
				if (!_isStarted && !_isActivationWithModel)
				{
					_model = value;
					return;
				}
				
				if (_isActivationWithModel)
				{
					enabled = null != value;
				}
				
				OnApplyModel(value);
				
				_model = value;
				
				if (null != _model)
				{
					_model.AddObserver(this);
					OnModelChanged(_model);
				}
			}
		}
		
		protected MGScheduledComponentWithModel()
		{
			_isActivationWithModel = this.GetType().GetCustomAttributes(typeof(ActivationWithModelAttribute), true).Length > 0;
		}

		protected sealed override void Start ()
		{
			base.Start ();

			_isStarted = true;
			
			if (null != _model)
			{
				this.Model = _model;
			}

			OnStart();
		}
		
		protected virtual void OnStart ()
		{
		}
		
		protected abstract void OnModelChanged (T model);
		
		protected virtual void OnApplyModel (T model)
		{
		}
		
		#region Observer implementation
		public void OnObjectChanged (Observable observable)
		{
			if (observable is T)
			{
				OnModelChanged((T)observable);
			}
			else
			{
				OnModelChanged(Model);
			}
		}
		#endregion
		
		protected sealed override void OnDestroy ()
		{
			this.Model = null;
			base.OnDestroy();
		}
	}
}