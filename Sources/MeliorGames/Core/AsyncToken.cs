// 
//  AsyncToken.cs
//  
//  Author:
//       Denis Oleynik <denis@meliorgames.com>
// 
//  Copyright (c) 2013 Melior Games Inc.
// 
//  
using System;
using System.Collections.Generic;

namespace MeliorGames.Core
{
	public sealed class AsyncToken<T>
	{
		private readonly List<Responder<T>> _responders;
		
		private T _command;
		private bool _lockResponders = false;

		public AsyncToken (T command)
		{
			_responders = new List<Responder<T>>();
			_command = command;
		}
		
		public void AddResponder(Responder<T> responder)
		{
			if (_lockResponders)
			{
				MonoLog.LogWarning(MonoLogChannel.Core, "Add responder on fire response");
			}
			else
			{
				_responders.Add(responder);
			}
		}
		
        public void RemoveResponder(Responder<T> responder)
        {
			if (_lockResponders)
			{
				MonoLog.LogWarning(MonoLogChannel.Core, "Remove responder on fire response");
			}
			else
			{
            	_responders.Remove( responder );
			}
        }

		internal void FireResponse()
		{
			_lockResponders = true;

			foreach(Responder<T> responder in _responders)
			{
				try
				{
					responder.result(_command);
				}
				catch(Exception e)
				{
					MonoLog.LogException(e);
				}
			}

			_lockResponders = false;
		}
		
		internal void FireFault()
		{
			_lockResponders = true;

			foreach(Responder<T> responder in _responders)
			{
				if(responder.fault != null)
					responder.fault(_command);
			}

			_lockResponders = false;
		}
	}
}