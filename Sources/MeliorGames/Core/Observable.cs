// 
//  Observable.cs
//  
//  Author:
//       Denis Oleynik <denis@meliorgames.com>
// 
//  Copyright (c) 2013 Melior Games Inc.
// 
//  
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using MeliorGames.Data.Serialization;
using System.Runtime.Serialization;

namespace MeliorGames.Core
{
	
	public interface IObservable
	{
		void SetChanged();
		void AddObserver(IObserver observer);
		void RemoveObserver(IObserver observer);
	}
	
	[Serializable]
	public abstract class Observable:BinarySerializableObject, IObservable
	{
		[NonSerialized]
		private List<IObserver> _observers;

		public Observable ()
		{
		}

		public Observable (SerializationInfo info, StreamingContext context):base(info, context)
		{
		}
		
		[XmlIgnore]
		public bool IsChanged
		{
			get;
			private set;
		}

		public void SetChanged()
		{
			IsChanged = true;
			
			if(_observers != null)
			{
				for(int i = _observers.Count-1; i >=0;i--)
					_observers[i].OnObjectChanged(this);
			}
		}
		
		public void Commit()
		{
			IsChanged = false;
		}
		
		public void SetChangedAndCommit()
		{
			SetChanged();
			
			Commit();
		}
		
		public void AddObserver(IObserver observer)
		{
			if(_observers == null)
				_observers = new List<IObserver>();
			
			_observers.Add(observer);
		}
		
		public void RemoveObserver(IObserver observer)
		{
			if(_observers == null)
				return;
			
			_observers.Remove(observer);
		}

		public void Clear()
		{
			if(_observers != null)
				_observers.Clear();
		}
	}
}