// 
//  MGMonoBehaviour.cs
//  
//  Author:
//       Denis Oleynik <denis@meliorgames.com>
// 
//  Copyright (c) 2013 Melior Games Inc.
// 
//  
using System;
using UnityEngine;

namespace MeliorGames.Core
{
	#if UNITY_EDITOR	
	public sealed class ShowDestroyAttribute:Attribute
	{
	}
	#endif

	public abstract class MGMonoBehaviour:MonoBehaviour
	{
		private Transform _transform;
		private bool _released = false;

		public MGMonoBehaviour ()
		{
		}
		
		public Transform CachedTransform
		{
			get
			{
				if(_transform == null)
					_transform = this.transform;
				
				return _transform;
			}
		}
		
		protected virtual void Start()
		{
		}

		private void OnApplicationQuit()
		{
			_released = true;
		}

		protected virtual void OnDestroy()
		{
			if(!_released)
			{
				OnReleaseResources();
			}

			_released = true;
		}
		
		protected virtual void OnReleaseResources()
		{
		}
	}
}