//  UIShadowPuzzleState.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	10/22/2015 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using MeliorGames.Core;

namespace MeliorGames.Core
{
	public abstract class StateWithComponentCommand<T> : StateCommand where T : MGMonoBehaviour
	{
		private T _controller;
		
		public T Controller
		{
			get
			{
				if( null == _controller )
				{
					_controller = GetComponent<T>();
				}
				return _controller;
			}
		}
	}
}