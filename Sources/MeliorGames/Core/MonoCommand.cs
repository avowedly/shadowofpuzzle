// 
//  MonoCommand.cs
//  
//  Author:
//       Denis Oleynik <denis@meliorgames.com>
// 
//  Copyright (c) 2013 Melior Games Inc.
// 
//  
using System;
using UnityEngine;
using MeliorGames.UI;

namespace MeliorGames.Core
{
	public sealed class SingletoneAttribute:Attribute
	{
	}
	
	public class MonoCommandScopeAttribute:Attribute
	{
		public readonly bool IsApplication;
		
		public MonoCommandScopeAttribute(bool application)
		{
			IsApplication = application; 
		}
	}
	
	public sealed class MonoCommandScopeApplicationAttribute:MonoCommandScopeAttribute
	{
		public MonoCommandScopeApplicationAttribute():base(true)
		{
			
		}
	}
		
	public sealed class MonoCommandScopeSceneAttribute:MonoCommandScopeAttribute
	{
		public MonoCommandScopeSceneAttribute():base(false)
		{
		}
	}	
	
	public sealed class MonoCommandCache:Attribute
	{
		
	}
	
	public interface ICommand
	{
		 void OnStart(object[] args);
	}
	
	public enum CommandScope
	{
		Application,
		Scene,
		GameObject
	}
	
	public abstract class MonoCommandTriggerAttribute:Attribute
	{
		public virtual void OnCommandStart(MonoCommand command, object[] args)
		{
		}
	}

	public abstract class MonoCommand:MonoScheduledBehaviour
	{
		private object[] _args;
		
		internal CommandScope Scope;
		
		bool _running = false;
		bool _started = false;
		bool _resourcesReleased = false;
					
		
		private bool _success;
		
		public bool FinishedWithSuccess
		{
			get
			{
				return _success;
			}
		}
		
		public bool IsRunning
		{
			get
			{
				return _running && _started;
			}
		}

		public bool IsFinishing
		{
			get
			{
				return !_running;
			}
		}

		public MonoCommand ()
		{
			_running = true;

//			MonoLog.Log(MonoLogChannel.Core, String.Format("Created command {0}",this.GetType().Name));			
		}
		
		protected override void Start()
		{
			if(_running)
			{
				_started = true;
				
				object[] attributes = this.GetType().GetCustomAttributes(true);
				
				foreach(object attribute in attributes)
				{
					if(attribute is MonoCommandTriggerAttribute)
					{
						MonoCommandTriggerAttribute trigger = (MonoCommandTriggerAttribute)attribute;	
						
						try
						{
							trigger.OnCommandStart(this, _args);	
						}
						catch(Exception e)
						{
							MonoLog.LogException(MonoLogChannel.Core, e, "Unable to execute trigger {0}", trigger.GetType().Name);
						}
					}
				}
				
//				MonoLog.Log(MonoLogChannel.Core, String.Format("Executing command {0}",this.GetType().Name));
							
				try
				{
					OnStart(_args);
				}
				catch(Exception e)
				{
					MonoLog.LogException(MonoLogChannel.Core, e, "Unable to execute command {0}", this.GetType().Name);
				}
			}
		}
		
		
		protected abstract void OnStart(object[] args);

		protected override void Update()
		{
            if(_running)
            {
                OnUpdate();
            }
            
            if(_running)
            {
                base.Update();              
            }
		}
		
		
		protected virtual void OnUpdate()
		{
		}
		
		public static TCommand ExecuteOn<TCommand>(GameObject target, params object[] args)
			where TCommand:MonoCommand,new()
		{
			object[] attibutes = typeof(TCommand).GetCustomAttributes(true);

			bool oneItemOnObject = false;

			foreach(Attribute eachAttribute in attibutes)
			{
				if(eachAttribute is SingletoneAttribute)
				{
					oneItemOnObject = true;
				}
			}
			
			if(oneItemOnObject)
			{
				TCommand[] commands = target.GetComponents<TCommand>();

				foreach (TCommand command in commands)
				{
					if(!command.IsFinishing)
					{
						MonoLog.Log(MonoLogChannel.Core,"Found existing command " + command);

						return command;
					}
				}
			}

			TCommand result = target.AddComponent<TCommand>();
			
			result._args = args;
			result.Scope = CommandScope.GameObject;
			
			return result;
		}
		
		public static MonoCommand ExecuteOn(Type type, GameObject target, object[] args)
		{
			MonoCommand result = (MonoCommand)target.AddComponent(type);

			result._args = args;
			result.Scope = CommandScope.GameObject;
			
			return result;
		}
		
		public static TCommand Execute<TCommand>(params object[] args)
			where TCommand:MonoCommand,new()
		{
			GameObject mgGameObject = MonoSingleton.GetMGGameObject();
			
			object[] attibutes = typeof(TCommand).GetCustomAttributes(true);
			
			bool oneItemOnScene = false;
			CommandScope scope = CommandScope.Application;
			
			foreach(Attribute eachAttribute in attibutes)
			{
				if(eachAttribute is SingletoneAttribute)
				{
					oneItemOnScene = true;
				}
				else if(eachAttribute is MonoCommandScopeAttribute)
				{
					MonoCommandScopeAttribute monoCommandScopeAttribute = (MonoCommandScopeAttribute)eachAttribute;
					
					scope = monoCommandScopeAttribute.IsApplication ? CommandScope.Application : CommandScope.Scene;
				}
			}
			
			if(oneItemOnScene)
			{
				TCommand[] commands = (TCommand[])UnityEngine.Object.FindObjectsOfType(typeof(TCommand));

				foreach (TCommand command in commands)
				{
					if(command._running)
					{
						MonoLog.Log(MonoLogChannel.Core,"Found existing command " + command);

						return command;
					}
				}
			}

			GameObject target = null;
						
			if( scope == CommandScope.Application )
			{
				target = new GameObject(typeof(TCommand).Name);
				target.transform.parent = mgGameObject.transform;

				UnityEngine.Object.DontDestroyOnLoad(target);				
			}
			else
            {
				if(UIManager.CurrentSceneController != null)
					target = UIManager.CurrentSceneController.gameObject;
            }

            TCommand result = null;

            if(target != null)
            {
			    result = ExecuteOn<TCommand>(target,args);
			
			    result.Scope = scope;
            }	

			return result;
		}

		public static TCommand Execute<TCommand>(Type type, params object[] args)
			where TCommand:MonoCommand,new()
		{
			GameObject mgGameObject = MonoSingleton.GetMGGameObject();
			
			object[] attibutes = typeof(TCommand).GetCustomAttributes(true);
			
			bool oneItemOnScene = false;
			CommandScope scope = CommandScope.Application;
			
			foreach(Attribute eachAttribute in attibutes)
			{
				if(eachAttribute is SingletoneAttribute)
				{
					oneItemOnScene = true;
				}
				else if(eachAttribute is MonoCommandScopeAttribute)
				{
					MonoCommandScopeAttribute monoCommandScopeAttribute = (MonoCommandScopeAttribute)eachAttribute;
					
					scope = monoCommandScopeAttribute.IsApplication ? CommandScope.Application : CommandScope.Scene;
				}
			}
			
			if(oneItemOnScene)
			{
				TCommand[] commands = (TCommand[])UnityEngine.Object.FindObjectsOfType(typeof(TCommand));
				
				foreach (TCommand command in commands)
				{
					if(!command.IsFinishing)
					{
						MonoLog.Log(MonoLogChannel.Core,"Found existing command " + command);
						
						return command;
					}
				}
			}
			
			GameObject target = null;
			
			if( scope == CommandScope.Application )
			{
				target = new GameObject(typeof(TCommand).Name);
				target.transform.parent = mgGameObject.transform;
				
				UnityEngine.Object.DontDestroyOnLoad(target);				
			}
			else
			{
				if(UIManager.CurrentSceneController != null)
					target = UIManager.CurrentSceneController.gameObject;
			}
			
			TCommand result = null;
			
			if(target != null)
			{
				result = (TCommand)ExecuteOn(type, target,args);
				
				result.Scope = scope;
			}	
			
			return result;
		}

		public static StateCommand ExecuteStateCommand(Type type, params object[] args)
		{
			return Execute<StateCommand> (type, args);
		}

		protected virtual void OnFinishCommand(bool success = true){}
		
	 	protected void FinishCommand(bool success = true)
		{
			if(_running)
			{
				_running = false;
				_success = success;
				
//				MonoLog.Log(MonoLogChannel.Core, "Finishing command " + this.GetType().Name);
				OnFinishCommand(success);
				
				if(_started)
				{
					ReleaseResources();					
				}

				this.hideFlags = HideFlags.HideInInspector;

				if(this.Scope == CommandScope.Application)
					UnityEngine.Object.Destroy(this.gameObject,1);
				else
					UnityEngine.Object.Destroy(this,1);
			}
		}
		
		private void ReleaseResources()
		{
			try
			{
				OnReleaseResources();
			}
			catch(Exception e)
			{
				MonoLog.LogException(MonoLogChannel.Core, e, "Error during release command resources");
			}
			finally
			{				
				_resourcesReleased =true;
			}	
		}

		private void OnApplicationQuit()
        {
            _resourcesReleased = true;
        }

		protected sealed override void OnDestroy()
        {
//            MonoLog.Log(MonoLogChannel.Core, "Command " + this.GetType().Name + " has beed destroyed");
            
            if(!_resourcesReleased && _started)
            {
                try
                {
                    OnReleaseResources();
                }
                catch(Exception e)
                {
                    MonoLog.LogException(MonoLogChannel.Core, e, "Error during release command resources");
                }

                _resourcesReleased = true;
            }
        }

		public void Terminate(bool success = false)
		{
			if(_running)
			{
				FinishCommand(success);
			}
		}
	}

	public abstract class MonoCommand<T>:MonoCommand
		where T:MonoCommand<T>,new()
	{
		public MonoCommand()
		{
			AsyncToken = new AsyncToken<T>((T)this);
		}
		
		public AsyncToken<T> AsyncToken
		{
			get;
			private set;
		}
		
		protected sealed override void OnFinishCommand (bool success = true)
		{
			if(success)
				AsyncToken.FireResponse();
			else
				AsyncToken.FireFault();
		}
	}
}