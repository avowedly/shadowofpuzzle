using System;
using UnityEngine;

namespace MeliorGames.Core
{
    public abstract class MonoSingleton:MonoScheduledBehaviour
    {
        public const string MG_NAME = "Melior Games";
        
        private static GameObject _mgGameObject;
        
        public static GameObject GetMGGameObject()
        {
            if (MonoSingleton._mgGameObject != null)
            {
                return MonoSingleton._mgGameObject;
            }
            
            MonoSingleton._mgGameObject = GameObject.Find(MG_NAME);
            
            if (MonoSingleton._mgGameObject == null)
            {
                MonoSingleton._mgGameObject = new GameObject(MG_NAME);
                UnityEngine.Object.DontDestroyOnLoad(MonoSingleton._mgGameObject);
            }
            return MonoSingleton._mgGameObject;
        }
    }

	internal class CustomInitializedMonoAttribute : Attribute
	{
	}

    public abstract class MonoSingleton<T> : MonoSingleton where T : MonoSingleton<T>
    {
        private static T _instance = null;
		private static bool _instantiated;

        public static T Instance
        {
            get
            {
				if(!_instantiated && typeof(T).GetCustomAttributes(typeof(CustomInitializedMonoAttribute), true).Length == 0)
                {
					_instantiated = true;

                    Initialize();
                }
                
                return _instance;
            }
        }
        
        public static void Initialize()
        {
            MonoBehaviour monoBehaviour = UnityEngine.Object.FindObjectOfType(typeof(T)) as MonoBehaviour;
            
            if (monoBehaviour == null)
            {
                GameObject mgGameObject = MonoSingleton.GetMGGameObject();
                
                GameObject gameObject = new GameObject(typeof(T).ToString());
                
                gameObject.AddComponent(typeof(T));
                gameObject.transform.parent = mgGameObject.transform;
                
                UnityEngine.Object.DontDestroyOnLoad(gameObject);
                
                _instance = gameObject.GetComponent<T>();
            }
            else
                _instance = monoBehaviour.GetComponent<T>();
        }
        
        private void Awake()
        {
            Init();
        }

		override protected sealed void Start()
		{
			base.Start();
			OnStart();
		}

        protected virtual void Init(){}
		protected virtual void OnStart(){}
        
        protected override void OnReleaseResources ()
		{
			base.OnReleaseResources ();
			_instance = null;
			_instantiated = false;
		}
    }
}