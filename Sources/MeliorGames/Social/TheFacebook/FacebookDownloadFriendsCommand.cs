// 
//  FacebookDownloadFriendsCommand.cs
//  
//  Author:
//       Denis Oleynik <denis@meliorgames.com>
// 
//  Copyright (c) 2013 Melior Games Inc.
// 
//  
using System;
using MeliorGames.Core;
using System.Collections.Generic;
using System.Collections;
using MeliorGames.Data;

namespace MeliorGames.Social.TheFacebook
{
	//[Singletone]
    [MonoCommandScopeScene]
	public sealed class FacebookDownloadFriendsCommand:MonoCommand<FacebookDownloadFriendsCommand>
	{
		public readonly List<FacebookUser> AllFriends;
		public readonly List<FacebookUser> InstalledOnly;

		public FacebookDownloadFriendsCommand ()
		{
			AllFriends = new List<FacebookUser>();
			InstalledOnly = new List<FacebookUser>();
		}
		
		protected override void OnStart (object[] args)
		{
			//FacebookManager.loginFailedEvent += OnFacebookLoginFailed;
			//FacebookManager.sessionOpenedEvent += OnFacebookSessionOpen;

            MonoCommand.Execute<FacebookInitializationCommand>().AsyncToken.AddResponder(
                new Responder<FacebookInitializationCommand>(OnFacebookLoginResult,OnFacebookLoginFailed));
		}

        private void OnFacebookLoginFailed(FacebookInitializationCommand command)
		{
			MonoLog.Log(MonoLogChannel.Facebook,"Unable to login to facebook.");

			FinishCommand(false);
		}
		
        private void OnFacebookLoginResult(FacebookInitializationCommand command)
		{	
            if(IsRunning)
            {
		 	    FB.API( "me/friends?fields=id,installed,name", Facebook.HttpMethod.GET, OnFriendsResponse ); 
            }
		}
		
		private void OnFriendsResponse(FBResult fbResult)
	    {   
            if(IsRunning)
            {
                if( !string.IsNullOrEmpty(fbResult.Error) )
    	        {
                    MonoLog.Log(MonoLogChannel.Facebook,"Unable to download friends.\n" + fbResult.Error);
    				
    				FinishCommand(false);
    				
    				return;
    	        }
    			
    			List<string> facebookIds = new List<string>();

                object result = Facebook.MiniJSON.Json.Deserialize( fbResult.Text );

    			if(result is IDictionary)
            	{
    	            foreach(IDictionary eachDictionary in (IList)((IDictionary)result)["data"])
    	            {	
    					FacebookUser facebookUser = new FacebookUser();
    					
    					facebookUser.Id = (string)eachDictionary["id"];
    					facebookUser.Name = (string)eachDictionary["name"];		
    					facebookUser.IsInstalled = eachDictionary.Contains("installed");
    					
    					AllFriends.Add( FacebookSettings.Map.Add( facebookUser ));
    					
    					if(facebookUser.IsInstalled)
    					{
    						InstalledOnly.Add(facebookUser);
    						
    						facebookIds.Add(facebookUser.Id);
    					}
    	            }				
    			}
    			else
    			{
    				MonoLog.Log(MonoLogChannel.Services, "The result does not mutch to expected type, received type is" + result.GetType());
    			}
    			
    			

				FinishCommand(true);
            }
			
	    }
		
	}
}
