// 
//  FacebookPostCommand.cs
//  
//  Author:
//       Denis Oleynik <denis@meliorgames.com>
// 
//  Copyright (c) 2013 Melior Games Inc.
// 
//  
using System;
using MeliorGames.Core;
using System.IO;
using UnityEngine;
using MeliorGames.Domain;

namespace MeliorGames.Social.TheFacebook
{
	[Singletone]
	public sealed class FacebookPostCommand:MonoCommand<FacebookPostCommand>
	{
		private const string kFacebookLink = "http://www.facebook.com";
		
		private string _facebookResult = string.Empty;
		
		public string FacebookResultText
		{
			private set
			{
				_facebookResult = value;
			}
			get
			{
				return _facebookResult;
			}
		}
		
		private PostParameters _postParameters;
		
		protected override void OnStart (object[] args)
		{
			_postParameters = (PostParameters)args[0];
			MonoCommand.Execute<FacebookInitializationCommand>().AsyncToken.AddResponder(new Responder<FacebookInitializationCommand>(
				OnFacebookInitializationResult,OnFacebookInitializationFault)); 
		}
		
		protected override void OnReleaseResources ()
		{
			base.OnReleaseResources ();
		}
		
		private void CallbackPost(FBResult result)
		{
			FacebookResultText = result.Text;
			if( string.IsNullOrEmpty( result.Error) && FB.IsLoggedIn )
			{
				FinishCommand(result.Text.Contains("posted"));
			}
			else 
				FinishCommand(false);
		}
		
		private void PostPhotoWithMessage() 
		{
			byte[] photo = File.ReadAllBytes(Photo.GetFilePath(_postParameters.Photo));
			
			var wwwForm = new WWWForm();
			wwwForm.AddBinaryData("image", photo);
			wwwForm.AddField("message", _postParameters.Message);

			FB.API("me/photos", Facebook.HttpMethod.POST, CallbackPost, wwwForm);
		}
		
		private void PostMessage() 
		{
			string photoPath = string.Empty;
			string actionLink = string.Empty;
			string actionName = string.Empty;
			
			if(!string.IsNullOrEmpty(_postParameters.PhotoPath))
				photoPath = _postParameters.PhotoPath;
			
			if(!string.IsNullOrEmpty(_postParameters.Link))
			{
				actionLink = _postParameters.Link;
			}
			FB.Feed(
				toId: string.Empty,
				link: kFacebookLink,
				linkName: _postParameters.Subject,
				linkCaption: string.Empty,
				linkDescription: _postParameters.Message,
				picture: photoPath,
				mediaSource: string.Empty,
				actionName: actionName,
				actionLink: actionLink,
				reference: string.Empty,
				properties: null,
				callback: CallbackPost
				);
		}
		
		private void OnFacebookInitializationResult(FacebookInitializationCommand command)
		{
			if(_postParameters.Photo != null)
				PostPhotoWithMessage();
			else
				PostMessage();
		}
		
		private void OnFacebookInitializationFault(FacebookInitializationCommand command)
		{
			FinishCommand(false);
		}		
		
	}
}