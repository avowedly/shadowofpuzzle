// 
//  FacebookInitializationCommand.cs
//  
//  Author:
//       Denis Oleynik <denis@meliorgames.com>
// 
//  Copyright (c) 2013 Melior Games Inc.
// 
//  
using System;
using MeliorGames.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ShadowPuzzles;

namespace MeliorGames.Social.TheFacebook
{
	//[Singletone]
	public sealed class FacebookInitializationCommand:MonoCommand<FacebookInitializationCommand>
	{		
		private static bool _initialized;

		public FacebookUser User
		{
			get;
			private set;
		}
		
		public FacebookInitializationCommand ()
		{
		}
		
		protected override void OnStart (object[] args)
		{
            if(!FB.IsLoggedIn )
			{
				if( !_initialized )
				{
					if( Application.internetReachability != NetworkReachability.NotReachable )
					{
                		FB.Init(OnInitComplete, OnHideUnity);
					}
					else
					{
						FinishCommand( false );
					}
				}
				else
				{
					OnInitComplete();
				}
			}
            else if(FacebookSettings.CurrentUser == null)
			{
				DownloadProfile();
			}
			else
			{
				User = FacebookSettings.CurrentUser;
                FinishCommand();
			}
		}

        private void OnInitComplete()
        {
			_initialized = true;

			MonoLog.Log(MonoLogChannel.Facebook,"Facebook initialized");

            if(FB.IsLoggedIn)
            {
                DownloadProfile();
            }
            else
			{
				MonoLog.Log(MonoLogChannel.Facebook,"Trying to login");

				FB.Login("publish_actions,user_games_activity,user_friends", OnFacebookLogin);
			}
        }

        private void DownloadProfile()
        {
			MonoLog.Log(MonoLogChannel.Facebook,"Downloading profile");

			if(UnityEngine.Application.internetReachability != NetworkReachability.NotReachable)
			{
				FB.API( "me", Facebook.HttpMethod.GET, OnGraphResult );
			}
			else
				FinishCommand( false );
        }

        private void OnHideUnity(bool isGameShown)
        {
            Debug.Log("Is game showing? " + isGameShown);
        }
		
		protected override void OnReleaseResources ()
		{
	
		}

		private void OnFacebookLogin(FBResult result)
		{
            if(!string.IsNullOrEmpty(result.Error))
            {
                MonoLog.Log(MonoLogChannel.Facebook,"Unable to login to facebook.\n" + result.Error);
			
			    FinishCommand( false );
            }
            else
			{
				MonoLog.Log(MonoLogChannel.Facebook,"Logged to faceboo, AT:" + FB.AccessToken);

                DownloadProfile();
			}
		}

		private void OnGraphResult(FBResult fbResult)
		{
            if( !string.IsNullOrEmpty( fbResult.Error ) )
	        {
                MonoLog.Log(MonoLogChannel.Facebook,"Unable to download profile.\n" + fbResult.Error);
				
				FinishCommand( false );
				
				return;
	        }

            object result = Facebook.MiniJSON.Json.Deserialize( fbResult.Text );
            	
            if(result is IDictionary)
            {
				IDictionary profile = (IDictionary)result;
//				IList data = (IList)map["data"];
//				IDictionary profile = (IDictionary)data[0];

    			try
    			{				
    				FacebookUser facebookUser = new FacebookUser();
    				facebookUser.Id = (string)profile["id"];
    				facebookUser.Name = (string)profile["name"];
					facebookUser.IsInstalled = true;

					facebookUser = FacebookSettings.Map.Add( facebookUser );
							
					if(profile.Contains("gender"))
    					facebookUser.IsFemale =  "female".Equals( profile["gender"] );
    				
    				this.User = facebookUser;

					FacebookSettings.CurrentUser = facebookUser;
					AppModel.Instance.FacebookPlayer = facebookUser;
					AppModel.Instance.SetChanged();
					FinishCommand();
    			}
    			catch(Exception e)
    			{					
    				MonoLog.Log(MonoLogChannel.Facebook,e);
    				
    				FinishCommand( false );
    			}
            }
		}
	}
}