// 
//  FacebookDownloadProfileImageCommand.cs
//  
//  Author:
//       Denis Oleynik <denis@meliorgames.com>
// 
//  Copyright (c) 2013 Melior Games Inc.
// 
//  
using System;
using MeliorGames.Core;


namespace MeliorGames.Social.TheFacebook
{
    public class FacebookProfileImage:DynamicImage
    {
        public readonly string FacebookId;
        
        public FacebookProfileImage(string facebookId)
        {
            FacebookId = facebookId;
            
            this.FileName = string.Format("fb.{0}.png",facebookId);
        }
    }
    
    public sealed class FacebookDownloadProfileImageCommand:DownloadManagerCommand
    {
        public FacebookDownloadProfileImageCommand ()
        {
        }
        
        protected override void Download ()
        {
            FacebookProfileImage facebookProfileImage = (FacebookProfileImage)File;
            
            string url = string.Format("http://graph.facebook.com/{0}/picture?type=square", facebookProfileImage.FacebookId);

            MonoLog.Log(MonoLogChannel.DownloadManager,"Initiating download file " + url);

            StartCoroutine(LoadURL(url));
        }
    }
}

