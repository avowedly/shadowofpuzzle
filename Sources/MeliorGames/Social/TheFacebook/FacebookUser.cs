// 
//  FacebookUser.cs
//  
//  Author:
//       Denis Oleynik <denis@meliorgames.com>
// 
//  Copyright (c) 2013 Melior Games Inc.
// 
//  
using System;
using MeliorGames.Domain;
using System.Collections.Generic;

namespace MeliorGames.Social.TheFacebook
{
	public sealed class FacebookUser : DomainObject
	{
		public String Name;
		public bool IsInstalled;
		public bool IsFemale;
		public int Score;
		public List<string> SomeOtherData;
		
		public FacebookUser ()
		{
			SomeOtherData = new List<string>();
		}
	}
}