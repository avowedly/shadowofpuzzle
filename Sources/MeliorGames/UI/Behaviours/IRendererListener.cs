﻿using UnityEngine;
using System.Collections;

namespace MeliorGames.UI.Behaviours
{
	public interface IRendererListener 
	{
		 void OnBecameVisible();
	
		 void OnBecameInvisible();
	}
}
