// 
//  UIDiagnostic.cs
//  
//  Author:
//       Denis Oleynik <denis@meliorgames.com>
// 
//  Copyright (c) 2013 Melior Games Inc.
// 
//  
using System;
using UnityEngine;

namespace MeliorGames.UI.Diagnostics
{
	public enum DiagnosticColor
	{
		Blue,
		White
	}
	
	public abstract class DiagnosticAttribute:Attribute
	{		
		protected readonly Color _color;

		public GUIStyle Style
		{
			get;
			protected set;
		}
		
		public DiagnosticAttribute():this(DiagnosticColor.White)
		{ 			
		}
		
		public DiagnosticAttribute(DiagnosticColor color)
		{
			Style = new GUIStyle();	

			if(color == DiagnosticColor.Blue)
			{
				Style.normal.textColor = Color.blue;
			}
			else
			{
				Style.normal.textColor = Color.white;
			}

			_color = Style.normal.textColor;
		}

		public abstract void OnUpdate();	
		public abstract void OnStart();

		public virtual void OnRelease ()
		{
		}
	}	
}