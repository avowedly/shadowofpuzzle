// 
//  MemoryCounterAttribute.cs
//  
//  Author:
//       Denis Oleynik <denis@meliorgames.com>
// 
//  Copyright (c) 2013 Melior Games Inc.
// 
//  
using System;
using MeliorGames.UI.Diagnostics;

namespace MeliorGames.UI.Diagnostics
{
	public sealed class MemoryCounterAttribute:DiagnosticAttribute
	{
		public MemoryCounterAttribute (DiagnosticColor color):base(color)
		{
		}
		
		public override void OnUpdate ()
		{
			
		}
		
		public override void OnStart ()
		{
			
		}
		
		public override string ToString ()
		{	
			return string.Format("{0}",
			                     System.GC.GetTotalMemory(true) / 1024 / 1024);
		}
	}
}