// 
//  UIPopUpController.cs
//  
//  Author:
//       Denis Oleynik <denis@meliorgames.com>
// 
//  Copyright (c) 2013 Melior Games Inc.
// 
//  
using System;
using UnityEngine;
using UnityEngine.UI;
using MeliorGames.Core;

namespace MeliorGames.UI
{
	public interface IUIPopUpControllerHandler
	{
		void PopUpDidFinish(UIPopUpController controller);
	}

	internal sealed class UIPopUpSceneVirtualParent:UIPopUpController
	{
		private readonly System.Object _result;
		
	 	public UIPopUpSceneVirtualParent(System.Object result)
		{
			_result = result;
		}		
		
		public override object Result
		{
			get 
			{
				return _result;
			}
		}

		public override void OnStart (object[] args)
		{
		}
	}
	
	public abstract class UIPopUpController : MonoScheduledBehaviour
	{
		private Canvas _popUpCanvas;
		private bool _isStarted;
		private bool _isClosed;

		[SerializeField]
		private Button _btnClose;

		internal int SortingOrder
		{
			get
			{
				return PopUpCanvas.sortingOrder;
			}
			set
			{
				PopUpCanvas.sortingOrder = value;
			}
		}

		public Canvas PopUpCanvas
		{
			get
			{
				if( _popUpCanvas == null )
				{
					_popUpCanvas = GetComponentInChildren<Canvas>();
				}
				return _popUpCanvas;
			}
		}
		
		public event Action<UIPopUpController> Closed;

		protected virtual void OnEnable()
		{
			PopUpCanvas.worldCamera = FindObjectOfType<PopUpCamera>().camera;
		}

		public virtual void OnStart( object[] args )
		{
			_isStarted = true;
			AddGlass();
			_btnClose.onClick.AddListener( () => SoundManager.Instance.PlayTapEffect() );
			_btnClose.onClick.AddListener( Close );
		}

		protected override void OnReleaseResources ()
		{
			base.OnReleaseResources ();
			_btnClose.onClick.RemoveAllListeners();
		}

		protected virtual void Close()
		{
			if( _isClosed == false && _isStarted )
			{
				_isClosed = true;
				BeforePopUpClose();
				UIManager.ClosePopUp(this);
				if( null != Closed )
				{
					Closed(this);	
				}
			}
		}

		public virtual System.Object Result
		{
			get
			{
				return String.Empty;
			}
		}

		protected virtual void BeforePopUpClose()
		{}

		private void AddGlass()
		{
			UIPopUpGlass glass = PrefabFactory.CreateInstance<UIPopUpGlass>();
			glass.transform.SetParent( PopUpCanvas.transform, false );
			glass.transform.SetAsFirstSibling();
		}
	}
}