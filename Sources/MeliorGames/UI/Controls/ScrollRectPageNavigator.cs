﻿using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using MeliorGames.Extentions;

namespace MeliorGames.UI.Controls
{
	[RequireComponent(typeof(ScrollRect))]
	public sealed class ScrollRectPageNavigator : MGComponent,IBeginDragHandler,IEndDragHandler
	{
		private enum NavigationType
		{
			Horizontal,
			Vertical
		}

		public event Action<ScrollRectPageNavigator> Changed;
		public event Action<ScrollRectPageNavigator> NavigateStart;
		public event Action<ScrollRectPageNavigator> NavigateFinish;

		private bool _needToUpdatePageNumber;
		private Vector2 _startDragPosition;
		private int _pageNumberInDragStarting;
		private float _pageNavigatorSensitivity;
		private ScrollRect _area;

		[SerializeField]
		private NavigationType _navigationType;
		[SerializeField]
		private float _movingScrollTime = 0.6f;
		[SerializeField]
		private iTween.EaseType _movingScrollAnimType = iTween.EaseType.easeOutBack;
		[SerializeField]
		private float _pageSize;
		[SerializeField]
		private int _totalPages;
		[SerializeField]
		private Vector2 _contentOffset;
		[SerializeField]
		private int _startingPage = 0;
		[SerializeField]
		private float _scrollSensitivityCoeff = 8;
		[SerializeField]
		private bool _isUsePagePositioning = true;

		private ScrollRect Area
		{
			get
			{
				if( _area == null )
				{
					_area = GetComponent<ScrollRect>();
				}
				return _area;
			}
		}

		public int PageNumber
		{
			private set;
			get;
		}

		public float PageSize
		{
			set
			{
				_pageSize = value;
				CalculateDimenstions();
			}
			private get
			{
				return _pageSize;
			}
		}

		public int TotalPages
		{
			set
			{
				_totalPages = value;
				CalculateDimenstions();
			}
			private get
			{
				return _totalPages;
			}
		}
	
		public Vector2 ContentOffset
		{
			set
			{
				_contentOffset = value;
				CalculateDimenstions();
			}
			get
			{
				return _contentOffset;
			}
		}

		private float _navigationDirection
		{
			get
			{
				return _navigationType == NavigationType.Vertical ? 1f : -1f;
			}
		}

		private float _targetScrollPosition
		{
			get
			{
				return _pageSize * PageNumber * _navigationDirection;
			}
		}

		protected override void OnStart ()
		{
			base.OnStart ();
			Area.horizontal = _navigationType == NavigationType.Horizontal;
			Area.vertical = _navigationType == NavigationType.Vertical;
			Area.inertia = false;
			Area.movementType = ScrollRect.MovementType.Unrestricted;
			CalculateDimenstions();
			MoveToPage( _startingPage, false );
		}

		private void CalculateDimenstions ()
		{
			if( _navigationType == NavigationType.Vertical )
			{
				Area.content.sizeDelta =
					new Vector2( Area.content.sizeDelta.x, 
					            ( TotalPages + 1 ) * _pageSize - ContentOffset.y );
			}
			else
			{
				Area.content.sizeDelta =
					new Vector2(  ( TotalPages + 1 ) * _pageSize - ContentOffset.x, 
					            Area.content.sizeDelta.y );
			}
			_pageNavigatorSensitivity = PageSize / _scrollSensitivityCoeff;
			UpdatePageNumber();
		}

		private float ScrollPosition
		{
			set
			{
				if( _navigationType == NavigationType.Vertical )
				{
					Area.content.anchoredPosition = new Vector2(
						Area.content.anchoredPosition.x, value );
				}
				else
				{
					Area.content.anchoredPosition = new Vector2(
						value, Area.content.anchoredPosition.y );
				}
			}
			get
			{
				if( _navigationType == NavigationType.Vertical )
				{
					return Area.content.anchoredPosition.y - ContentOffset.y;
				}
				else
				{
					return Area.content.anchoredPosition.x - ContentOffset.x;
				}
			}
		}

		private void UpdatePageNumber()
		{
			int prevPageNumber = PageNumber;
			PageNumber = Mathf.RoundToInt( _navigationDirection * ScrollPosition / _pageSize );
			PageNumber = Mathf.Clamp( PageNumber, 0, TotalPages - 1 );
			if( prevPageNumber != PageNumber && Changed != null )
			{
				Changed(this);
			}
		}

		#if UNITY_EDITOR || UNITY_STANDALONE
		private void Update ()
		{

			if( ( Input.GetKeyUp( KeyCode.DownArrow ) && _navigationType == NavigationType.Vertical )
			   || ( Input.GetKeyUp( KeyCode.RightArrow ) && _navigationType == NavigationType.Horizontal ) )
			{
				MoveToPage( PageNumber + 1, true );
			}

			if( ( Input.GetKeyUp( KeyCode.UpArrow ) && _navigationType == NavigationType.Vertical )
			   || ( Input.GetKeyUp( KeyCode.LeftArrow ) && _navigationType == NavigationType.Horizontal ) )
			{
				MoveToPage( PageNumber - 1, true );
			}
		}
		#endif

		private void LateUpdate()
		{
			if( _needToUpdatePageNumber )
			{
				UpdatePageNumber();
			}
		}

		private void OnUpdateValue( float value )
		{
			ScrollPosition = value;
		}

		private Vector2 GetPagePosition ( int pageNumber )
		{
			if( _navigationType == NavigationType.Horizontal )
			{
				return new Vector2( - Area.content.sizeDelta.x / 2 + _pageSize * pageNumber + ContentOffset.x, 
				                   ContentOffset.y );
			}
			else
			{
				return new Vector2(  ContentOffset.x,
				                   Area.content.sizeDelta.y / 2 - _pageSize * pageNumber + ContentOffset.y );
			}
		}

		private void CalculateTargetPageNumberByDragLength( Vector2 endDragPoint )
		{
			int targetPage = _pageNumberInDragStarting;
			if( _navigationType == NavigationType.Horizontal )
			{
				targetPage -= (int) ( ( endDragPoint.x - _startDragPosition.x ) / _pageNavigatorSensitivity );
			}
			else if( _navigationType == NavigationType.Vertical )
			{
				targetPage += (int) ( ( endDragPoint.y - _startDragPosition.y ) / _pageNavigatorSensitivity );
			}
			MoveToPage( targetPage, true );
		}

		public void AddPageObjectToScoll( int pageNumber, RectTransform row )
		{
			row.SetParent( Area.content, false );
			row.anchoredPosition = GetPagePosition( pageNumber );
		}

		public void MoveToPage( int pageNumber, bool withAnim )
		{
			pageNumber = Mathf.Clamp( pageNumber, 0, TotalPages - 1 );
			iTween.Stop( gameObject );
			this.PageNumber = pageNumber;
			if( withAnim )
			{
				gameObject.ValueTo( ScrollPosition, _targetScrollPosition, _movingScrollAnimType, _movingScrollTime, 0f, OnUpdateValue );
			}
			else
			{
				ScrollPosition = _targetScrollPosition;
			}
			Changed.SafeInvoke( this );
		}

		#region IEndDragHandler implementation
		
		public void OnEndDrag (PointerEventData eventData)
		{
			_needToUpdatePageNumber = false;
			if( _isUsePagePositioning || PageNumber == 0 || PageNumber == TotalPages - 1 )
			{
				CalculateTargetPageNumberByDragLength( eventData.position );
			}
			if(NavigateFinish != null)
			{
				NavigateFinish(this);
			}
		}
		
		#endregion
		
		#region IBeginDragHandler implementation
		
		public void OnBeginDrag (PointerEventData eventData)
		{
			iTween.Stop( gameObject );
			_needToUpdatePageNumber = true;
			_startDragPosition = eventData.position;
			_pageNumberInDragStarting = PageNumber;
			if(NavigateStart != null)
			{
				NavigateStart(this);
			}
		}
		
		#endregion
	}
}