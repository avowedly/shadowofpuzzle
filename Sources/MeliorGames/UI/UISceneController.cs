// 
//  UISceneController.cs
//  
//  Author:
//       Denis Oleynik <denis@meliorgames.com>
// 
//  Copyright (c) 2013 Melior Games Inc.
// 
//  
using System;
using UnityEngine.EventSystems;

namespace MeliorGames.UI
{
	public sealed class UISceneNameAttribute:Attribute
	{
		public readonly String Name;
		
		public UISceneNameAttribute(string name)
		{
			this.Name = name;
		}
	}

	public abstract class UISceneController : UIController
	{
		private EventSystem _eventSystem;

		public UISceneController ()
		{
		}				
		
		protected override void OnInitialize ()
		{
			base.OnInitialize ();
			_eventSystem = EventSystem.current;
#if OFFLINE_MODE
			if( FindObjectOfType<PopUpCamera>() == null )
			{
				PrefabFactory.CreateInstance<PopUpCamera>();
			}
#else
			PrefabFactory.CreateInstance<PopUpCamera>();
#endif
		}

		internal override void OnAfterStart ()
		{
			base.OnAfterStart ();
		}

		protected sealed override void Update ()
		{
			base.Update ();
			OnUpdate();
		}

		public void ShowBlocker ()
		{
			if (null != _eventSystem)
			{
				_eventSystem.enabled = false;
			}
		}

		public void HideBlocker ()
		{
			if (null != _eventSystem)
			{
				_eventSystem.enabled = true;
			}
		}

		public bool IsShowedBlocker
		{
			get
			{
				return !_eventSystem.enabled;
			}
		}

		protected virtual void OnUpdate ()
		{
		}
	}
}