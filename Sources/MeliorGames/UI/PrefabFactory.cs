// 
//  PrefabFactory.cs
//  
//  Author:
//       Denis Oleynik <denis@meliorgames.com>
// 
//  Copyright (c) 2013 Melior Games Inc.
// 
using System;
using System.Collections.Generic;
using UnityEngine;
using MeliorGames.Core;

namespace MeliorGames.UI
{
	public static class PrefabFactory
	{
		static PrefabFactory ()
		{
		}

		public static T CreateInstance<T>()
			where T:MonoBehaviour
		{
			return (T)CreateInstance(typeof(T));
		}

		public static Component CreateInstance(Type type)
		{
			string path = PathByType(type);

			GameObject gameObject = InstatiatePrefab(path);

			if (null != gameObject)
			{
				gameObject.name = type.Name;
			}
			
			return gameObject.GetComponent(type);
		}

		public static GameObject CreateInstance(Enum key)
		{
			String path = PathByType(key.GetType())+"/"+key.ToString();

			GameObject gameObject = InstatiatePrefab(path);

			if (null != gameObject)
			{
				gameObject.name = key.ToString();
			}
			
			return gameObject;
		}
		
		public static T CreateInstance<T>(GameObject prefab)
			where T:Component
		{
			GameObject gameObject = (GameObject)GameObject.Instantiate(prefab);

			gameObject.name = typeof(T).Name;
			
			return gameObject.GetComponent<T>();
		}

		public static GameObject InstatiatePrefab(string path)
		{
			UnityEngine.Object prefab = Resources.Load(path);

			if (null == prefab)
			{
				MonoLog.LogWarning(MonoLogChannel.Core, "Not found prefab by path "+path);
				return null;
			}

			return (GameObject)GameObject.Instantiate(prefab);
		}

		private static string PathByType(Type type)
		{
			string[] splitPath = StringUtil.Split(type.Namespace, "UI.");
			string path = splitPath[splitPath.Length-1];
			
			string name = "."+type.Name.Replace("Controller", string.Empty);

			if (name.EndsWith("View"))
			{
				name = name.Remove(name.Length-"View".Length, "View".Length);
			}

			path = path.Replace(name, string.Empty) + name;
			
			path = path.Replace(".", "/");
			
			return path;
		}
	}
}