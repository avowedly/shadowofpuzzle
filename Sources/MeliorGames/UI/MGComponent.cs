// 
//  MGComponent.cs
//  
//  Author:
//       Denis Oleynik <denis@meliorgames.com>
// 
//  Copyright (c) 2013 Melior Games Inc.
// 
//  
using System;
using UnityEngine;
using MeliorGames.Core;

namespace MeliorGames.UI
{
	public class ActivationWithModelAttribute : Attribute
	{
	}

	public abstract class MGComponent:MGMonoBehaviour
	{
		private Boolean _isReleased = true;

		protected override sealed void Start()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			OnStart();
		}

		public Boolean IsReleased
		{
			get
			{
				return _isReleased;
			}
			private set
			{
				_isReleased = value;
			}
		}
		
		public new bool enabled
		{
			get
			{
				return gameObject.activeInHierarchy;
			}
			set
			{
				base.enabled = value;
				gameObject.SetActive(value);
				
				if (true)
				{
					IsReleased = false;
				}
			}
		}
		
		public void Release ()
		{
			enabled = false;
			IsReleased = true;
		}

		protected virtual void OnStart()
		{
		}

		protected virtual void OnEnable()
		{
		}
		
		protected virtual void OnDisable()
		{
		}

		public MGComponent ()
		{
		}

		protected virtual void OnUpdate()
		{
		}

		private void Update()
		{
			#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
			#endif

			OnUpdate();
		}
	}

	abstract public class MGComponentWithModel<T>:MGComponent,IObserver where T : Observable
	{
		private Boolean _isStarted = false;
		private Boolean _isActivationWithModel;
		private T _model;

		public T Model
		{
			protected get
			{
				return _model;
			}
			set
			{
				if (null != _model)
				{
					_model.RemoveObserver(this);
				}

				if (!_isStarted && !_isActivationWithModel)
				{
					_model = value;
					return;
				}

				if (_isActivationWithModel)
				{
					enabled = null != value;
				}

				OnApplyModel(value);

				_model = value;

				if (null != _model)
				{
					_model.AddObserver(this);
					OnModelChanged(_model);
				}
			}
		}

		protected MGComponentWithModel()
		{
			_isActivationWithModel = this.GetType().GetCustomAttributes(typeof(ActivationWithModelAttribute), true).Length > 0;
		}

		protected override void OnStart ()
		{
			base.OnStart ();

			_isStarted = true;

			if (null != _model)
			{
				this.Model = _model;
			}
		}

		protected abstract void OnModelChanged (T model);

		protected virtual void OnApplyModel (T model)
		{
		}

		#region Observer implementation
		public void OnObjectChanged (Observable observable)
		{
			if (observable is T)
			{
				OnModelChanged((T)observable);
			}
			else
			{
				OnModelChanged(Model);
			}
		}
		#endregion

		protected sealed override void OnDestroy ()
		{
			this.Model = null;
			base.OnDestroy();
		}
	}

	abstract public class MGComponentWithModelView<T>:MGComponentWithModel<T> where T : Observable
	{
		abstract protected IObserver ObserverView{get;}

		protected override void OnApplyModel (T model)
		{
			base.OnApplyModel (model);

			if (null != Model)
			{
				Model.RemoveObserver(ObserverView);
			}

			if(null == model)
				return;

			model.AddObserver(ObserverView);
			ObserverView.OnObjectChanged(model);
		}

		protected override void OnModelChanged (T model)
		{
		}
	}
}