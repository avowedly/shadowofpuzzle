// 
//  CamerFollowPlayerCommand.cs
//  
//  Author:
//       Denis Oleynik <denis@meliorgames.com>
// 
//  Copyright (c) 2013 Melior Games Inc.
// 
//  
using System;
using MeliorGames.Core;
using UnityEngine;

namespace MeliorGames.UI.Commands
{
	public sealed class SmoothFollowCommand:StateCommand
	{		
		private Parameters _parameters;


	
		[SerializeField]
		private Vector2 _offset;

		public bool LockY = true;
			
		public const float DEFAULT_SMOOTH_TIME =  0.3f;

		public sealed class Parameters
		{
			public Transform Target;
			public Transform Destination;
			public float SmoothTime;
			public Vector3 Offset;
			
			public Parameters (Transform target, Transform destination, float smoothTime = DEFAULT_SMOOTH_TIME)
			{
				this.Target = target;
				this.Destination = destination;
				this.SmoothTime = smoothTime;
			}
			public Parameters (Transform target, Transform destination, float smoothTime, Vector2 offset):
				this(target,destination,smoothTime)
			{
				this.Offset = offset;
			}
		}
		
		public SmoothFollowCommand ()
		{
			
		}
		
		protected override void OnStart (object[] args)
		{
			base.OnStart(args);
			
			_parameters = (Parameters)args[0];

	
			_offset = _parameters.Offset;
		}
		
		private void LateUpdate ()
		{
			Vector2 velocity = Vector2.zero;

			float positionX = Mathf.SmoothDamp( _parameters.Target.position.x, _parameters.Destination.position.x + _offset.x, ref velocity.x, _parameters.SmoothTime);
			float positionY = _parameters.Target.position.y;

			if(!LockY)
				positionY = Mathf.SmoothDamp( _parameters.Target.position.y, _parameters.Destination.position.y - _offset.y, ref velocity.y, _parameters.SmoothTime);
		
			_parameters.Target.position = new Vector3(positionX, positionY, _parameters.Target.position.z);
		}
	}
}

