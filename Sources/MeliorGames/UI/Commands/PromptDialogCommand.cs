using MeliorGames.Core;
using System;
using Prime31;
using UnityEngine;

namespace MeliorGames.UI.Commands
{
	public sealed class PromptDialogCommand:MonoCommand<PromptDialogCommand>
	{
		#if !UNITY_ANDROID || OFFLINE_MODE
		private const int kWindowWidth = 300;
		private const int kWindowHeight = 200;
		private const int kBtnWidth = 60;
		private const int kBtnHeight = 30;
		private const int kBtnTop = 150;
		private const int kMessageOffsetX = 50;
		private const int kMessageOffsetY = 20;
		#endif
		private Parameters _parameters;
		private bool _clickedOk;
		
		public bool IsPositiveClick
		{
			get
			{
				return _clickedOk;
			}
		}
		
		public sealed class Parameters
		{
			public readonly string FirstBtn;
			public readonly string SecondBtn;
			public readonly string Message;
			public readonly string Title;
			
			public Parameters(string title, string message, string firstButton )
			{
				this.Message = message;
				this.FirstBtn = firstButton;
				this.SecondBtn = string.Empty;
				this.Title = title;
			}

			public Parameters(string title, string message, string firstButton, string secondButton )
			{
				this.Message = message;
				this.FirstBtn = firstButton;
				this.SecondBtn = secondButton;
				this.Title = title;
			}	

			public Parameters(string message):this("Message", message, "OK")
			{
			}
		}		
		
		public PromptDialogCommand ()
		{
		}
		
		protected override void OnStart (object[] args)
		{
			_parameters = (Parameters)args[0];
			#if UNITY_ANDROID && !OFFLINE_MODE
			EtceteraAndroidManager.alertButtonClickedEvent += OnAlertButtonClicked;

			if( _parameters.SecondBtn == string.Empty )
			{
				EtceteraAndroid.showAlert(_parameters.Title,_parameters.Message,_parameters.FirstBtn);
			}
			else
			{
				EtceteraAndroid.showAlert(_parameters.Title,_parameters.Message,_parameters.FirstBtn, _parameters.SecondBtn );
			}
			#else
			UIManager.ShowBlocker();
			#endif
		}

#if !UNITY_ANDROID || OFFLINE_MODE
		private void OnGUI()
		{
			if( IsRunning )
			{
				Rect modalWindowRect = new Rect( Screen.width / 2 - kWindowWidth / 2,
				                                Screen.height / 2 - kWindowHeight / 2,
				                                kWindowWidth,
				                                kWindowHeight );
				GUI.ModalWindow( 0, modalWindowRect, DoMyWindow, _parameters.Title );
			}
		}

		private void DoMyWindow(int windowID)
		{
			GUIStyle centeredStyle = GUI.skin.GetStyle("Label");
			centeredStyle.alignment = TextAnchor.MiddleCenter;
			GUI.Label(new Rect( kMessageOffsetX, kMessageOffsetY,
			                   kWindowWidth - kMessageOffsetX * 2, kWindowHeight - kMessageOffsetY * 2 ),
			          _parameters.Message, centeredStyle );
			int firstBtnPosX = _parameters.SecondBtn == string.Empty ?
				kWindowWidth / 2 - kBtnWidth / 2 : kWindowWidth / 3 - kBtnWidth / 2;
			if(GUI.Button(new Rect( firstBtnPosX, kBtnTop, kBtnWidth, kBtnHeight ),_parameters.FirstBtn))
			{
				OnAlertButtonClicked( _parameters.FirstBtn );
			}
			if( _parameters.SecondBtn != string.Empty && 
			   GUI.Button(new Rect( kWindowWidth * 2 / 3 - kBtnWidth / 2, kBtnTop, kBtnWidth, kBtnHeight ),_parameters.SecondBtn))
			{
				OnAlertButtonClicked( _parameters.SecondBtn );
			}
		}
#endif
		
		private void OnAlertButtonClicked (string name)
		{
			#if !UNITY_ANDROID || OFFLINE_MODE
			UIManager.HideBlocker();
			#endif
			_clickedOk = name == _parameters.FirstBtn;
			FinishCommand();
		}
		
		protected override void OnReleaseResources ()
		{
			#if !UNITY_EDITOR && UNITY_ANDROID
			EtceteraAndroidManager.alertButtonClickedEvent -= OnAlertButtonClicked;
			#endif
		}		
	}
}