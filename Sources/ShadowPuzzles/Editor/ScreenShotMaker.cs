﻿using UnityEngine;
using System.Collections;
using MeliorGames.UI;

namespace ShadowPuzzles.Editor
{
	public class ScreenShotMaker : MGComponent
	{
		protected override void OnUpdate ()
		{
			base.OnUpdate ();
			if( Input.GetKeyDown( KeyCode.S ) )
			{
				Application.CaptureScreenshot( Application.persistentDataPath + "_1.png", 2 );
			}
		}
	}
}
