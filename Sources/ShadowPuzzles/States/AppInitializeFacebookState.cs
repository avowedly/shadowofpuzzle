//  AppInitializeFacebookState.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	12/18/2015 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using MeliorGames.Core;
using MeliorGames.Social.TheFacebook;

namespace ShadowPuzzles.States
{
	public sealed class AppInitializeFacebookState : AppState
	{
		public AppInitializeFacebookState ()
		{
		}
		
		protected override void OnStart (object[] args)
		{
			FacebookSettings.Namespace = AppConfig.Facebook.FacebookNamespace;
			
			MonoCommand.Execute<FacebookInitializationCommand>().AsyncToken.AddResponder(
				new Responder<FacebookInitializationCommand>(OnFacebookInitializeResult,OnFacebookInitializeFault));
		}
		
		private void OnFacebookInitializeResult(FacebookInitializationCommand command)
		{
			if(IsRunning)
			{
				
				AppModel.Instance.FacebookPlayer = command.User;
				
				MonoCommand.Execute<FacebookDownloadFriendsCommand>().AsyncToken.AddResponder(
					new Responder<FacebookDownloadFriendsCommand>(OnFacebookDownloadFriendsResult,OnFacebookDownloadFriendsFault));
			}
		}
		
		private void OnFacebookInitializeFault(FacebookInitializationCommand command)
		{
			if(IsRunning)
			{
				
				AppController.Instance.StateMachine.ApplyState<AppReadyState>();
			}
		}
		
		private void OnFacebookDownloadFriendsResult(FacebookDownloadFriendsCommand command)
		{
			if(IsRunning)
			{
				MonoCommand.Execute<FacebookDownloadScoresCommand>().AsyncToken.AddResponder(
					new Responder<FacebookDownloadScoresCommand>(OnFacebookDownloadScoresResult,OnFacebookDownloadScoresFault));
			}
		}
		
		private void OnFacebookDownloadFriendsFault(FacebookDownloadFriendsCommand command)
		{
			if(IsRunning)
				AppController.Instance.StateMachine.ApplyState<AppReadyState>();
		}
		
		private void OnFacebookDownloadScoresResult(FacebookDownloadScoresCommand command)
		{
			if(IsRunning)
			{
				AppController.Instance.StateMachine.ApplyState<AppReadyState>();
			}
		}
		
		private void OnFacebookDownloadScoresFault(FacebookDownloadScoresCommand command)
		{
			AppController.Instance.StateMachine.ApplyState<AppReadyState>();
		}
	}
}