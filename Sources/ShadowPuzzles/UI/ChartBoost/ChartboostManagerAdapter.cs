//  ChartboostManagerAdapter.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	10/19/2015 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using Prime31;
using System;
using UnityEngine;

namespace ShadowPuzzles.UI.ChartBoost
{
	public static class ChartboostManagerAdapter
	{
		public static event Action<string> didCacheInterstitialEvent;
		public static event Action<string,string> didFailToCacheInterstitialEvent;
		public static event Action<string,string> didFinishInterstitialEvent;
		public static event Action<string> didCacheMoreAppsEvent;
		public static event Action<string,string> didFailToCacheMoreAppsEvent;
		public static event Action<string,string> didFinishMoreAppsEvent;
		public static event Action<string> didCacheRewardedVideoEvent;
		public static event Action<string,string> didFailToLoadRewardedVideoEvent;
		public static event Action<string,string> didFinishRewardedVideoEvent;
		public static event Action<int> didCompleteRewardedVideoEvent;
		public static event Action<string> didFailToLoadUrlEvent;

		static ChartboostManagerAdapter()
		{
			#if UNITY_IPHONE
			AbstractManager.initialize( typeof( ChartboostManager ) );
			ChartboostManager.didCacheInterstitialEvent += OnCacheInterstitialEvent;
			ChartboostManager.didFailToCacheInterstitialEvent += OnFailToCacheInterstitialEvent;
			ChartboostManager.didFinishInterstitialEvent += OnFinishInterstitialEvent;
			ChartboostManager.didCacheMoreAppsEvent += OnCacheMoreAppsEvent;
			ChartboostManager.didFailToCacheMoreAppsEvent += OnFailToCacheMoreAppsEvent;
			ChartboostManager.didFinishMoreAppsEvent += OnFinishMoreAppsEvent;
			ChartboostManager.didCacheRewardedVideoEvent += OnCacheRewardedVideoEvent;
			ChartboostManager.didFailToLoadRewardedVideoEvent += OnFailToLoadRewardedVideoEvent;
			ChartboostManager.didFinishRewardedVideoEvent += OnFinishRewardedVideoEvent;
			ChartboostManager.didCompleteRewardedVideoEvent += OnCompleteRewardedVideoEvent;
			#elif UNITY_ANDROID
			AbstractManager.initialize( typeof( ChartboostAndroidManager ) );
			ChartboostAndroidManager.didCacheInterstitialEvent += OnCacheInterstitialEvent;
			ChartboostAndroidManager.didFailToCacheInterstitialEvent += OnFailToCacheInterstitialEvent;
			ChartboostAndroidManager.didFinishInterstitialEvent += OnFinishInterstitialEvent;
			ChartboostAndroidManager.didCacheMoreAppsEvent += OnCacheMoreAppsEvent;
			ChartboostAndroidManager.didFailToCacheMoreAppsEvent += OnFailToCacheMoreAppsEvent;
			ChartboostAndroidManager.didFinishMoreAppsEvent += OnFinishMoreAppsEvent;
			ChartboostAndroidManager.didCacheRewardedVideoEvent += OnCacheRewardedVideoEvent;
			ChartboostAndroidManager.didFailToLoadRewardedVideoEvent += OnFailToLoadRewardedVideoEvent;
			ChartboostAndroidManager.didFinishRewardedVideoEvent += OnFinishRewardedVideoEvent;
			ChartboostAndroidManager.didCompleteRewardedVideoEvent += OnCompleteRewardedVideoEvent;
			ChartboostAndroidManager.didFailToLoadUrlEvent += OnFailToLoadUrlEvent;
			#endif
		}

		private static void OnCacheInterstitialEvent( string arg1 )
		{
			if( didCacheInterstitialEvent != null )
			{
				didCacheInterstitialEvent( arg1 );
			}
		}

		private static void OnFailToCacheInterstitialEvent( string arg1, string arg2 )
		{
			if( didFailToCacheInterstitialEvent != null )
			{
				didFailToCacheInterstitialEvent( arg1, arg2 );
			}
		}

		private static void OnFinishInterstitialEvent( string arg1, string arg2 )
		{
			if( didFinishInterstitialEvent != null )
			{
				didFinishInterstitialEvent( arg1, arg2 );
			}
		}

		private static void OnCacheMoreAppsEvent( string arg1 )
		{
			if( didCacheMoreAppsEvent != null )
			{
				didCacheMoreAppsEvent( arg1 );
			}
		}

		private static void OnFailToCacheMoreAppsEvent( string arg1, string arg2 )
		{
			if( didFailToCacheMoreAppsEvent != null )
			{
				didFailToCacheMoreAppsEvent( arg1, arg2 );
			}
		}

		private static void OnFinishMoreAppsEvent( string arg1, string arg2 )
		{
			if( didFinishMoreAppsEvent != null )
			{
				didFinishMoreAppsEvent( arg1, arg2 );
			}
		}

		private static void OnCacheRewardedVideoEvent( string arg1 )
		{
			if( didCacheRewardedVideoEvent != null )
			{
				didCacheRewardedVideoEvent( arg1 );
			}
		}

		private static void OnFailToLoadRewardedVideoEvent( string arg1, string arg2 )
		{
			Debug.Log( arg1 );
			Debug.Log( arg2 );
			if( didFailToLoadRewardedVideoEvent != null )
			{
				didFailToLoadRewardedVideoEvent( arg1, arg2 );
			}
		}

		private static void OnFinishRewardedVideoEvent( string arg1, string arg2 )
		{
			if( didFinishRewardedVideoEvent != null )
			{
				didFinishRewardedVideoEvent( arg1, arg2 );
			}
		}

		private static void OnCompleteRewardedVideoEvent( int arg1 )
		{
			if( didCompleteRewardedVideoEvent != null )
			{
				didCompleteRewardedVideoEvent( arg1 );
			}
		}

		private static void OnFailToLoadUrlEvent( string arg1 )
		{
			if( didFailToLoadUrlEvent != null )
			{
				didFailToLoadUrlEvent( arg1 );
			}
		}
	}
}