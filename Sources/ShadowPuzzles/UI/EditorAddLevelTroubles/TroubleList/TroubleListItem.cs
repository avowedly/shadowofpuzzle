//  TroubleListItem.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	11/03/2015 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using MeliorGames.UI;
using ShadowPuzzles.Domain.Troubles;
using UnityEngine.UI;
using UnityEngine;

namespace ShadowPuzzles.UI.EditorAddLevelTroubles.TroubleList
{
	public class TroubleListItem : MGComponentWithModel<TroubleInfo>
	{
		public Action<TroubleInfo> ClickedDelete;
		public Action<TroubleInfo> ClickedEdit;

		[SerializeField]
		private Text _txtDescription;
		[SerializeField]
		private Button _btnDelete;
		[SerializeField]
		private Button _btnEdit;

		protected override void OnStart ()
		{
			base.OnStart ();
			_btnEdit.onClick.AddListener( OnEditClick );
			_btnDelete.onClick.AddListener( OnDeleteClick );
		}

		protected override void OnReleaseResources ()
		{
			base.OnReleaseResources ();
			_btnEdit.onClick.RemoveListener( OnEditClick );
			_btnDelete.onClick.RemoveListener( OnDeleteClick );
		}

		#region implemented abstract members of MGComponentWithModel
		protected override void OnModelChanged (TroubleInfo model)
		{
			_txtDescription.text = model.Description;
		}
		#endregion

		private void OnEditClick ()
		{
			ClickedEdit.SafeInvoke( Model );
		}

		private void OnDeleteClick ()
		{
			ClickedDelete.SafeInvoke( Model );
		}
	}
}