//  FairyStoreState.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	01/11/2016 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using MeliorGames.Core;


namespace ShadowPuzzles.UI.FairyStore.States
{
	public class FairyStoreState : StateWithComponentCommand<FairyStoreSceneController>
	{
		public FairyStoreView View
		{
			get
			{
				return Controller.View;
			}
		}

		public FairyStoreModel Model
		{
			get
			{
				return Controller.Model;
			}
		}
	}
}