//  FairyStoreitem.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	01/11/2016 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using MeliorGames.UI;
using UnityEngine;
using ShadowPuzzles.UI.Game.Unit;
using Sources.ShadowPuzzles.Domain;
using MeliorGames.UI.Atlas;
using System.Collections.Generic;
using animationsyur;


namespace ShadowPuzzles.UI.FairyStore.Components
{
	public sealed class FairyStoreItem : MGComponent
	{
		private static Dictionary<FairyCostume, string> _mappingFairyToSpriteName;

		static FairyStoreItem()
		{
			_mappingFairyToSpriteName = new Dictionary<FairyCostume, string>();
			_mappingFairyToSpriteName.Add( FairyCostume.Mexico, FairyLockSkinsAtlas.FadeFairy_Egypt );
			_mappingFairyToSpriteName.Add( FairyCostume.Egypt, FairyLockSkinsAtlas.FadeFairy_Egypt );
			_mappingFairyToSpriteName.Add( FairyCostume.Kamboja, FairyLockSkinsAtlas.FadeFairy_Kamboja );
			_mappingFairyToSpriteName.Add( FairyCostume.Peru, FairyLockSkinsAtlas.FadeFairy_Peru );
		}

		[SerializeField]
		private FairyUnitController _fairy;
		[SerializeField]
		private ExtendedImage _imgLock;

		private FairyCostume _fairySkin;

		public FairyCostume FairySkin
		{
			set
			{
				_fairySkin = value;
				_imgLock.SetSprite( _mappingFairyToSpriteName[ FairySkin ] );
				_fairy.SetCostume( FairySkin );
			}
			private get
			{
				return _fairySkin;
			}
		}

		public bool IsLocked
		{
			set
			{
				_imgLock.gameObject.SetActive( value );
				_fairy.gameObject.SetActive( !value );
			}
		}

		public void PlayHappyAnim ()
		{
			_fairy.ApplyState( EFairyState.Happy );
		}

		public void PlayIdleAnim()
		{
			_fairy.ApplyState( EFairyState.Idle, false );
		}
	}
}