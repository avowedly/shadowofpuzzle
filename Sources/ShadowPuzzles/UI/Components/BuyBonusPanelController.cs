//  BuyBonusPanelController.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	01/29/2016 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using MeliorGames.UI;
using UnityEngine.UI;
using UnityEngine;
using MeliorGames.UI.Atlas;
using ShadowPuzzles.Domain.GameBonuses;
using System.Collections.Generic;
using ShadowPuzzles.UI.PopUps;


namespace ShadowPuzzles.UI.Components
{
	public class BuyBonusPanelController : MGComponentWithModel<AppModel>
	{
		public event Action<BuyBonusPanelController> Closed;

		private const string kPriceText = "Buy {0}";

		private static Dictionary<GameBonusType, string> _mappingBonusTypeToSpriteName;
		
		static BuyBonusPanelController()
		{
			_mappingBonusTypeToSpriteName = new Dictionary<GameBonusType, string>();
			_mappingBonusTypeToSpriteName.Add( GameBonusType.ResolveLevel, BonusesAtlas.IconBig_Resolve );
			_mappingBonusTypeToSpriteName.Add( GameBonusType.SelectCrouseType, BonusesAtlas.IconBig_Choose_Cruser );
			_mappingBonusTypeToSpriteName.Add( GameBonusType.ShowPicture, BonusesAtlas.IconBig_Tube_FinalPicture );
			_mappingBonusTypeToSpriteName.Add( GameBonusType.SwapElements, BonusesAtlas.IconBig_Switch );
		}

		[SerializeField]
		private Text _txtDescription;
		[SerializeField]
		private ExtendedButton _btnBuy;
		[SerializeField]
		private ExtendedButton _btnCoins;
		[SerializeField]
		private ExtendedButton _btnClose;
		[SerializeField]
		private ExtendedImage _imgBonus;
		[SerializeField]
		private Image _imgBackground;

		private GameBonusType _bonus;

		private int Price
		{
			get
			{
				return AppConfig.GameBonuses.GetInfo( _bonus ).Price;
			}
		}

		protected override void OnStart ()
		{
			base.OnStart ();
			_btnClose.onClick.AddListener( OnCloseClick );
			_btnBuy.onClick.AddListener( OnBuyClick );
			_btnCoins.onClick.AddListener( OnCoinsClick );
		}

		protected override void OnReleaseResources ()
		{
			base.OnReleaseResources ();
			_btnClose.onClick.RemoveListener( OnCloseClick );
			_btnBuy.onClick.RemoveListener( OnBuyClick );
			_btnCoins.onClick.RemoveListener( OnCoinsClick );
		}

		public void ShowPanel( GameBonusType bonusType, Transform parent, Vector2 shift )
		{
			iTween.Stop( gameObject );
			_bonus = bonusType;
			transform.SetParent( parent, false );
			transform.GetComponent<RectTransform>().anchoredPosition = shift;
			if( _mappingBonusTypeToSpriteName.ContainsKey( _bonus ) )
			{
				_imgBonus.SetSprite( _mappingBonusTypeToSpriteName[ _bonus ] );
			}
			else
			{
				MonoLog.Log( MonoLogChannel.Exceptions, "Need add all bonus sprite!" );
			}
			_txtDescription.text = AppConfig.GameBonuses.GetInfo( _bonus ).Description;
			_btnBuy.Label.text = string.Format( kPriceText, Price );
			Model = AppModel.Instance;
			var bacgroundPos = _imgBackground.transform.position;
			bacgroundPos.x = 0;
			_imgBackground.transform.position = bacgroundPos;
			gameObject.MoveAdd( new Vector3( 0, -0.2f, 1 ), 1.5f, 0f, iTween.EaseType.linear, LoopType.pingPong );
		}

		private void OnCloseClick ()
		{
			SoundManager.Instance.PlayTapEffect();
			iTween.Stop( gameObject );
			Closed.SafeInvoke( this );
			gameObject.SetActive( false );
			Destroy( gameObject );
		}

		private void OnBuyClick ()
		{
			SoundManager.Instance.PlayTapEffect();
			if( Model.Player.Coins >= Price )
			{
				Model.Player.GameBonuses[_bonus]++;
				Model.Player.Coins -= Price;
				Model.SetChanged();
			}
			else
			{
				UIManager.OpenPopUp<BuyGameCoinsPopUpController>();
			}
		}

		private void OnCoinsClick ()
		{
			SoundManager.Instance.PlayTapEffect();
			UIManager.OpenPopUp<BuyGameCoinsPopUpController>();
		}

		#region implemented abstract members of MGComponentWithModel

		protected override void OnModelChanged (AppModel model)
		{
			_btnCoins.Label.text = Model.Player.Coins.ToString();
		}

		#endregion
	}
}