//  MoveFairyToNextLevelCommand.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	12/23/2015 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using ShadowPuzzles.UI.Map.States;
using UnityEngine;
using animationsyur;

namespace ShadowPuzzles.UI.Map.Commands
{
	internal sealed class MoveFairyToNextLevelCommand : MapSceneState
	{
		public class Parametrs
		{
			public readonly float AnimTime;
			public readonly float AnimDelay;
			public readonly Vector3 TargetPos;

			public Parametrs( float animTime, float animDelay, Vector3 targetPos )
			{
				this.AnimTime = animTime;
				this.AnimDelay = animDelay;
				this.TargetPos = targetPos;
			}
		}

		private Parametrs _parametrs;

		protected override void OnStart (object[] args)
		{
			base.OnStart (args);
			_parametrs = args[0] as Parametrs;
			View.Fairy.ApplyState( EFairyState.Idle );
			View.Fairy.MoveTo( _parametrs.TargetPos, _parametrs.AnimTime, _parametrs.AnimDelay, iTween.EaseType.easeOutBack );
			ScheduleUpdate( _parametrs.AnimTime + _parametrs.AnimDelay, false );
		}

		protected override void OnScheduledUpdate ()
		{
			base.OnScheduledUpdate ();
			View.Fairy.ApplyState( EFairyState.Point );
			FinishCommand();
		}
	}
}