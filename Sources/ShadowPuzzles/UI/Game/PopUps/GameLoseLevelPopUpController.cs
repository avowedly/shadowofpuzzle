//  GameLoseLevelPopUpController.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	01/08/2016 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using MeliorGames.UI;
using UnityEngine.UI;
using UnityEngine;
using ShadowPuzzles.UI.Game.States;
using ShadowPuzzles.UI.PopUps;

namespace ShadowPuzzles.UI.Game.PopUps
{
	internal sealed class GameLoseLevelPopUpController : UIAnimPopUpController
	{
		[SerializeField]
		private Button _btnExit;
		[SerializeField]
		private Button _btnRestart;
		[SerializeField]
		private Text _txtLevelNumber;
		[SerializeField]
		private Text _txtOutOfStep;

		private GameSceneController _gameController;

		public override void OnStart (object[] args)
		{
			base.OnStart (args);
			_gameController = args[0] as GameSceneController;
			_txtLevelNumber.text = ( _gameController.Model.LevelNumber + 1 ) .ToString();
			_btnExit.onClick.AddListener( OnLeaveGameClick );
			_btnRestart.onClick.AddListener( OnRestartClick );
			if( args.Length > 1 && (bool)args[1] )
			{
				_txtOutOfStep.gameObject.SetActive( false );
			}
		}
		
		protected override void OnReleaseResources ()
		{
			base.OnReleaseResources ();
			_btnExit.onClick.RemoveAllListeners();
			_btnRestart.onClick.RemoveAllListeners();
		}

		private void OnRestartClick ()
		{
			SoundManager.Instance.PlayTapEffect();
			if( AppModel.Instance.Player.Lives > 1 )
			{
				_gameController.StateMachine.ApplyState<GameFinishLevelState>( GameFinishLevelState.FinishAction.Restart );
			}
			else
			{
				UIManager.OpenPopUp<NeedBuyLivesPopUpController>().Closed += ( popUp ) => {
					if( ( bool )popUp.Result )
					{
						_gameController.StateMachine.ApplyState<GameFinishLevelState>( GameFinishLevelState.FinishAction.Restart );
					}
				};
			}
		}

		private void OnLeaveGameClick ()
		{
			SoundManager.Instance.PlayTapEffect();
			_gameController.StateMachine.ApplyState<GameFinishLevelState>( GameFinishLevelState.FinishAction.Exit );
		}
	}
}