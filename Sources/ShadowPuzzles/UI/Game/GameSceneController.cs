//  GameSceneController.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	10/27/2015 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using MeliorGames.UI;
using ShadowPuzzles.Triggers;
using ShadowPuzzles.UI.Game;
using ShadowPuzzles.UI.Game.Strategy;
using MeliorGames.UI.Diagnostics;
using ShadowPuzzles.UI.Game.Crouser.Elements.Navigator;
using ShadowPuzzles.UI.Game.Elements.FingerNavigator;
using ShadowPuzzles.Domain;
using ShadowPuzzles.UI.EditorCrouse;
using ShadowPuzzles.UI.EditorPipe;
using ShadowPuzzles.UI.EditorMozaik;
using ShadowPuzzles.UI.Game.Unit;
using ShadowPuzzles.UI.Game.States;
using ShadowPuzzles.UI.Components;
using ShadowPuzzles.UI.Map;

namespace ShadowPuzzles.UI.Game
{
	[UISceneName("Game")]
	[WaitForAppReadyTrigger]
	[FPSCounter]
	public sealed class GameSceneController : UIShadowPuzzlesSceneController
	{
		public Action<bool> GameFinished;

		public GameModel Model
		{
			internal set;
			get;
		}
		
		public GameView View;
		public BonusesPanelController BonusesPanel;
		public CrouseNavigatorController CrouseNavigator;
		public FingerNavigatorController FingerNavigator;
		public DarkDemonUnitController Demon;
		public FairyUnitController Fairy;

		public GameSceneStrategy Strategy
		{
			private set;
			get;
		}

		protected override void OnStart (object[] args)
		{
			base.OnStart (args);
#if UNITY_EDITOR
			if( AppModel.Instance.CurrentLevel == null )
			{
				int loadingLevelNumber = 91;
				LevelStatisticInfo level = new LevelStatisticInfo( loadingLevelNumber,
				                                                   AppConfig.Map.GetLevelParametrs( loadingLevelNumber ));
				AppModel.Instance.CurrentGameType = level.Parametrs.GameType;
				AppModel.Instance.LevelNumberInLocation = level.Parametrs.NumberInLocation;
				AppModel.Instance.CurrentLevel = AppModel.Instance.Player.Locations[ (int)level.Parametrs.GameType ].
					GetLevel( level.Parametrs.NumberInLocation );
				MonoLog.Log( MonoLogChannel.Test, string.Format( "Load level {0}, type {1}, local number {2}",
				                                                loadingLevelNumber, level.Parametrs.GameType, level.Parametrs.NumberInLocation ) );
			}
#endif
			Strategy = new GameSceneStrategy( AppModel.Instance.CurrentGameType );
			StateMachine.ApplyState( Strategy.InitializeState );
		}
	}
}