//  GameTroubleController.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	11/03/2015 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using MeliorGames.UI;
using MeliorGames.Core;
using UnityEngine;
using ShadowPuzzles.UI.Game.Crouser.Elements.Navigator;

namespace ShadowPuzzles.UI.Game.Trouble
{
	public sealed class GameTroubleController : MGComponent, IStateMachineContainer
	{
		public Action WorkWithTroublesCompleted;
		public Action StartStepForTrouble;

		public readonly StateMachine StateMachine;

		[SerializeField]
		internal GameSceneController GameController;
		
		public GameTroubleController()
		{
			this.StateMachine = new StateMachine(this);
		}

		#region IStateMachineContainer implementation
		
		public void Next (StateCommand previousState)
		{
		}
		
		public GameObject GameObject
		{
			get
			{
				return gameObject;
			}
		}
		
		#endregion

		public CrouseNavigatorController CrouseNavigator
		{
			get
			{
				return GameController.CrouseNavigator;
			}
		}

		public void CheckTroubles()
		{
			if( StartStepForTrouble != null )
			{
				StartStepForTrouble();
			}
		}
	}
}