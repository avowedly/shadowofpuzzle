//  GameTroubleChangeCrouseTypeCommand.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	11/04/2015 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using ShadowPuzzles.Domain.Troubles;
using UnityEngine;
using ShadowPuzzles.Domain.Crouser;


namespace ShadowPuzzles.UI.Game.Trouble.Commands
{
	internal sealed class GameTroubleChangeCrouseTypeCommand : GameTroubleWithInfoCommand<TroubleChangeCrouserTypeInfo>
	{
		public override float EffectAnimTime
		{
			get 
			{
				return AppConfig.Troubles.ChangeCrouseTypeAnimTime;
			}
		}

		public override bool IsHasOppositeEffect 
		{
			get 
			{
				return true;
			}
		}

		public override int StepForOppositeEffect
		{
			get 
			{
				return TroubleInfo.StepForAction + TroubleInfo.DurationInStep;
			}
		}

		private CrouserType _previousCrouserType;

		protected override void StartMakeTrouble ()
		{
			PlayNavigatorAnim();
			_previousCrouserType = Controller.CrouseNavigator.Model.CrouserType;
			Controller.CrouseNavigator.Model.CrouserType = TroubleInfo.TargetCrouserType;
			Controller.CrouseNavigator.Model.SetChanged();
		}

		public override void MakeOppositeEffect ()
		{
			PlayNavigatorAnim();
			ScheduleUpdate( EffectAnimTime, false );
		}
		
		protected override void OnScheduledUpdate ()
		{
			base.OnScheduledUpdate ();
			if( Controller.CrouseNavigator.Model.CrouserType == TroubleInfo.TargetCrouserType )
			{
				Controller.CrouseNavigator.Model.CrouserType = _previousCrouserType;
				Controller.CrouseNavigator.Model.SetChanged();
			}
			FinishCommand();
		}

		private void PlayNavigatorAnim ()
		{
			const float kNavigatorRotateAmount = 45f;
			Controller.CrouseNavigator.View.gameObject.ShakeRotation( new Vector3( 0, 0, kNavigatorRotateAmount ),
			                                                   EffectAnimTime, 0f );
		}
	}
}