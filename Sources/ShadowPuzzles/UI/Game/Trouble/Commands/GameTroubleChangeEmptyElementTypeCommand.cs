//  GameTroubleChangeEmptyElementTypeCommand.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	11/06/2015 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using ShadowPuzzles.UI.Game.Mozaik.Commands;
using ShadowPuzzles.Domain.Troubles;
using ShadowPuzzles.UI.Game.Elements;

namespace ShadowPuzzles.UI.Game.Trouble.Commands
{
	internal sealed class GameTroubleChangeEmptyElementTypeCommand : GameTroubleWithInfoCommand<TroubleChangeEmptyElementTypeInfo>
	{
		public override float EffectAnimTime
		{
			get
			{
				return AppConfig.AnimationTiming.PuzzleMovingAnimTime;
			}
		}

		protected override void StartMakeTrouble ()
		{
			Controller.StateMachine.Execute<MovePuzzleToEmptyElementCommand>
				( FieldController.Instance.LstPuzzlesElements[ TroubleInfo.TargetIndex ].Model.Position );
			ScheduleUpdate( EffectAnimTime, false );
		}

		protected override void OnScheduledUpdate ()
		{
			base.OnScheduledUpdate ();
			FinishCommand();
		}
	}
}