//  PuzzleElementState.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	10/22/2015 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using ShadowPuzzles.Domain;
using MeliorGames.Core;


namespace ShadowPuzzles.UI.Game.Elements.PuzzleElement.States
{
	public abstract class PuzzleElementState : StateWithComponentCommand<PuzzleElementController>
	{
		public PuzzleElementView View
		{
			get
			{
				return Controller.View;
			}
		}

		public PuzzleElementInfo Model
		{
			get
			{
				return Controller.Model;
			}
		}
	}
}