//  PuzzleElement.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	10/22/2015 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using MeliorGames.UI;
using ShadowPuzzles.Domain;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace ShadowPuzzles.UI.Game.Elements
{
	public abstract class PuzzleElementView : MGComponentWithModel<PuzzleElementInfo>, IPointerDownHandler
	{
		internal event Action Pressed;

		public GameObject IceElement;
		public Image ImgShadow;

		protected override void OnStart ()
		{
			base.OnStart ();
			if( IceElement != null )
			{
				IceElement.gameObject.SetActive( false );
			}
		}

		#region IPointerDownHandler implementation
		
		public void OnPointerDown (PointerEventData eventData)
		{
			Pressed.SafeInvoke();
		}
		
		#endregion
	}
}