//  ShowFinalMozaikPictureCommand.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	02/11/2016 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using MeliorGames.Core;

namespace ShadowPuzzles.UI.Game.Commands.BonusCommands
{
	[Singletone]
	internal sealed class ShowFinalMozaikPictureCommand : GameCommand
	{
		protected override void OnStart (object[] args)
		{
			base.OnStart (args);
			View.MozaikFullImage.gameObject.SetActive( true );
		}

		protected override void OnReleaseResources ()
		{
			base.OnReleaseResources ();
			View.MozaikFullImage.gameObject.SetActive( false );
		}
	}
}