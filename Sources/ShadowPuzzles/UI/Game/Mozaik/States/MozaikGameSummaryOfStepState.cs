//  MozaikGameSummaryOfStepState.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	11/05/2015 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using ShadowPuzzles.UI.Game.Elements;
using ShadowPuzzles.UI.Game.States;
using ShadowPuzzles.Domain;
using System.Collections.Generic;


namespace ShadowPuzzles.UI.Game.Mozaik.States
{
	internal sealed class MozaikGameSummaryOfStepState : GameSummaryOfStepState
	{
		private static Dictionary<int, int> _mappingPlayerStepLose;
		private static Dictionary<int, int> _mappingPlayerStepWin;
		
		static MozaikGameSummaryOfStepState()
		{
			_mappingPlayerStepLose = new Dictionary<int, int>();
			_mappingPlayerStepLose.Add( 0, 2 );
            _mappingPlayerStepLose.Add( 1, 3 );
			_mappingPlayerStepLose.Add( 2, 4 );
			_mappingPlayerStepLose.Add( 3, 5 );
			_mappingPlayerStepLose.Add( 4, 6 );
			_mappingPlayerStepLose.Add( 5, 7 );
			_mappingPlayerStepLose.Add( 6, 8 );
			
			_mappingPlayerStepWin = new Dictionary<int, int>();
			_mappingPlayerStepWin.Add( 0, 0 );
            _mappingPlayerStepWin.Add( 1, 2 );
			_mappingPlayerStepWin.Add( 2, 2 );
			_mappingPlayerStepWin.Add( 3, 2 );
			_mappingPlayerStepWin.Add( 4, 3 );
			_mappingPlayerStepWin.Add( 5, 3 );
			_mappingPlayerStepWin.Add( 6, 3 );
			_mappingPlayerStepWin.Add( 7, 4 );
		}

		#region implemented abstract members of GameSummaryOfStepState
		
		protected override bool CheckPlayerWin ()
		{
			for( int i = 0; i < FieldController.Instance.LstPuzzlesElements.Count; i++ )
			{
				PuzzleElementInfo currentElement = FieldController.Instance.LstPuzzlesElements[i].Model;
				PositionInField winnerPos = Model.CurrentLevel.LstPuzzleWinnerPos[ i ];
				if( currentElement.Position != winnerPos && currentElement.Position != PositionInField.OutsidePos )
				{
					return false;
				}
			}
			return true;
		}

		protected override bool IsPlayerSoonLose ()
		{
			if( _mappingPlayerStepLose.ContainsKey( Model.StepsLeft ) )
			{
				return GetElementsInNoneFinalPosCount().Count >= _mappingPlayerStepLose[ Model.StepsLeft ];
			}
			return false;
		}
		
		protected override bool IsPlayerSoonWin ()
		{
			int stepLeftMin = Model.StepsLeft;
			while( stepLeftMin > 0 && !_mappingPlayerStepWin.ContainsKey( stepLeftMin ) )
			{
				stepLeftMin--;
			}
			return GetElementsInNoneFinalPosCount().Count <= _mappingPlayerStepWin[ stepLeftMin ];
		}

		#endregion

		private PuzzleElementInfo GetElementByTypeFromModel( int type )
		{
			foreach( PuzzleElementInfo element in AppModel.Instance.CurrentLevel.LstPuzzleElements )
			{
				if( type == element.Type )
				{
					return element;
				}
			}
			return null;
		}
	}
}