//  CrouseNavigatorController.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	10/23/2015 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using ShadowPuzzles.Domain.Crouser;
using UnityEngine;
using ShadowPuzzles.Domain;
using ShadowPuzzles.UI.Game.Crouser.Elements.Navigator.Strategy;
using ShadowPuzzles.UI.Game.Crouser.Elements.Navigator.States;
using ShadowPuzzles.UI.Game.Elements;
using System;
using MeliorGames.UI;
using MeliorGames.Core;

namespace ShadowPuzzles.UI.Game.Crouser.Elements.Navigator
{
	public class CrouseNavigatorController : MGComponent, IStateMachineContainer
	{
		public Action PuzzlesPositionsChanged;
		public Action<PositionInField> CrouserPositionChanged;
		public Action StartChangeOfPuzzlePosition;
		
		public readonly StateMachine StateMachine;

		[SerializeField]
		internal CrouseNavigatorView View;

		public CrouseNavigatorModel Model
		{
			private set;
			get;
		}

		internal NavigatorStrategy Strategy
		{
			private set;
			get;
		}

		public CrouseNavigatorController()
		{
			StateMachine = new StateMachine( this );
		}

		public void InitializeCrouser( CrouserType type, bool withChangedPosition = true )
		{
			Model = new CrouseNavigatorModel( new PositionInField( 0, 1 ), type );
			View.Model = Model;
			if( withChangedPosition )
			{
				View.CachedTransform.position = GetTargetPos();
			}
			Strategy = new NavigatorStrategy( Model );
		}

		public void MakeStep( PositionInField targetPos )
		{
			StateMachine.ApplyState<CrouseMoveToPointState>( targetPos );
		}
		
		public Vector3 GetTargetPos()
		{
			Vector2 upperLeftPos = FieldController.Instance.GetPosForElement( Model.PositionOfUpperLeftAncor ); 
			Vector2 lowerRightPos = FieldController.Instance.GetPosForElement( Model.PositionOfLowerRightAncor );
			return new Vector3( upperLeftPos.x + ( lowerRightPos.x - upperLeftPos.x ) / 2,
			                    lowerRightPos.y + ( upperLeftPos.y - lowerRightPos.y ) / 2,
			                   CachedTransform.position.z );
		}

		public void TurnOn (bool on)
		{
			if( on )
			{
				StateMachine.ApplyState<CrouseNavigatorReadyState>();
			}
			else
			{
				StateMachine.ApplyState<CrouseNavigatorStopState>();
			}
		}

		#region IStateMachineContainer implementation
		
		public virtual void Next (StateCommand previousState)
		{
		}
		
		public GameObject GameObject
		{
			get
			{
				return gameObject;
			}
		}
		
		#endregion
	}
}