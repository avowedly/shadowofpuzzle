//  CrouseArrowView.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	01/28/2016 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using MeliorGames.UI;
using MeliorGames.Extentions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;


namespace ShadowPuzzles.UI.Game.Crouser.Elements.Navigator
{
	public sealed class CrouseArrowView : MGComponent
	{
		private const float kMinValue = 0.1f;
		private const float kMaxValue = 1f;

		[SerializeField]
		private List<Image> _lstImgArrow;

		private bool _isIncrease;

		protected override void OnStart ()
		{
			base.OnStart ();
			_isIncrease = false;
			PlayAnim();
		}

		private void PlayAnim()
		{
			float startValue = _isIncrease ? kMinValue : kMaxValue;
			float endVolue = _isIncrease ? kMaxValue : kMinValue;
			gameObject.ValueTo( startValue, endVolue, iTween.EaseType.linear, 1f, 0f, OnUpdateAnim, PlayAnim );
			_isIncrease = !_isIncrease;
		}

		private void OnUpdateAnim( float value )
		{
			var tmp = _lstImgArrow[0].color;
			tmp.a = value;
			_lstImgArrow.ForEach( arrow => arrow.color = tmp );
		}
	}
}