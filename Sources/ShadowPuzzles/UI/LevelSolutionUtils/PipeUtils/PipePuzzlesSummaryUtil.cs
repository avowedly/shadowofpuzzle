//  PuzzlesRatingUtilForPipe.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	12/09/2015 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using ShadowPuzzles.Domain;
using ShadowPuzzles.UI.Game.Pipe.Elements;
using ShadowPuzzles.UI.Game.Elements;
using ShadowPuzzles.Domain.Pipe;

namespace ShadowPuzzles.UI.LevelSolutionUtils.PipeUtils
{
	public class PipePuzzlesSummaryUtil
	{
		private const int kCheckWinRecursionValue = 2;

		private static Dictionary<MoveDirection, MoveDirection> _mappingOppositeDirections;

		private List<PipePuzzleElementController> _pipesWithWater;
		private List<PipePuzzleElementController> _allPipes;
		private List<PipePuzzleElementController> _puzzleExits;
		private List<PositionInField> _pipeWithWaterStartPositions;
		
		private bool _isWaterLeak = false;
		private PipeLevelInfo _model;
		private PositionInField _positionEmptyElement;
		
		static PipePuzzlesSummaryUtil()
		{
			_mappingOppositeDirections = new Dictionary<MoveDirection, MoveDirection>();
			_mappingOppositeDirections.Add( MoveDirection.None, MoveDirection.None );
			_mappingOppositeDirections.Add( MoveDirection.Left, MoveDirection.Right );
			_mappingOppositeDirections.Add( MoveDirection.Up, MoveDirection.Down );
			_mappingOppositeDirections.Add( MoveDirection.Right, MoveDirection.Left );
			_mappingOppositeDirections.Add( MoveDirection.Down, MoveDirection.Up );
		}

		public PipePuzzlesSummaryUtil( PipeLevelInfo pipeModel )
		{
			_model =  pipeModel;
		}
		
		public bool CheckPlayerWin ( PositionInField emptyPos, bool withPlayWaterAnim = true )
		{
			_positionEmptyElement = emptyPos;
			_isWaterLeak = false;
			DetectPipeState();
			SaveStartWaterPipePos();
			if( withPlayWaterAnim )
			{
				PlayWaterAnim();
			}
			if( _isWaterLeak )
			{
				return false;
			}
			foreach( PipePuzzleElementController element in _puzzleExits )
			{
				if( !_pipesWithWater.Contains( element ) )
				{
					return false;
				}
			}
			return true;
		}

		public bool IsThisWaterPipeElement( PuzzleElementController element )
		{
			return _pipesWithWater.Contains( element as PipePuzzleElementController );
		}

		public PositionInField StartPipeElementWithWaterPos( PuzzleElementController element )
		{
			return _pipeWithWaterStartPositions[ _pipesWithWater.IndexOf( element as PipePuzzleElementController ) ];
		}
        
		private void DetectPipeState ()
		{
			_pipesWithWater = new List<PipePuzzleElementController> ();
			_allPipes = new List<PipePuzzleElementController> ();
			_puzzleExits = new List<PipePuzzleElementController> ();
			foreach (PuzzleElementController element in FieldController.Instance.LstPuzzlesElements)
			{
				if( _positionEmptyElement != element.Model.Position )
				{
					PipePuzzleElementController pipeElement = element as PipePuzzleElementController;
					if( pipeElement.Model.Position == _model.SourcePos )
					{
						_pipesWithWater.Add (pipeElement);
					}
					else if( pipeElement.IsOneEnter )
					{
						_puzzleExits.Add (pipeElement);
					}
					_allPipes.Add (element as PipePuzzleElementController);
				}
			}
			int lastWaterPipeIndex = 0;
			while( _pipesWithWater.Count > lastWaterPipeIndex ) 
			{
				LetTheWater (lastWaterPipeIndex);
				lastWaterPipeIndex++;
			}
		}
		
		private void LetTheWater ( int startFrom )
		{
			PipePuzzleElementController element = _pipesWithWater[ startFrom ];
			foreach( MoveDirection dir in element.PipeDirections )
			{
				PipePuzzleElementController neigboardElement = 
					FieldController.Instance.GetNeigboardElement
						( element.Model.Position, dir ) as PipePuzzleElementController;
				if( neigboardElement != null && neigboardElement.Model.Position != _positionEmptyElement
				   && neigboardElement.PipeDirections.Contains( _mappingOppositeDirections[ dir ] ) )
				{
					if( !_pipesWithWater.Contains( neigboardElement ) )
					{
						_pipesWithWater.Add( neigboardElement );
					}
				}
				else
				{
					_isWaterLeak = true;
				}
			}
		}

		private void PlayWaterAnim ()
		{
			_allPipes.ForEach( element => element.TurnWater( IsHaveWater( element ) ) );
		}

		private void SaveStartWaterPipePos ()
		{
			_pipeWithWaterStartPositions = new List<PositionInField>();
			_pipesWithWater.ForEach( element => _pipeWithWaterStartPositions.Add( element.Model.Position ) );
		}

		private bool IsHaveWater( PipePuzzleElementController pipeElement )
		{
			return _pipesWithWater.Contains( pipeElement ) && ( !_isWaterLeak || !pipeElement.IsOneEnter
			                                                   || pipeElement.Model.Position == _model.SourcePos );
		}
	}
}