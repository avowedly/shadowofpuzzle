//  UIPopUpControllerWithAppModel.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	12/07/2015 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using MeliorGames.UI;
using MeliorGames.Core;
using UnityEngine.UI;
using UnityEngine;


namespace ShadowPuzzles.UI.PopUps
{
	internal abstract class UIPopUpControllerWithAppModel : UIAnimPopUpController, IObserver
	{
		private const string kTextCoins = "coins: {0}";
		private const string kTextLives = "lives: {0}";

		[SerializeField]
		private Text _txtCoins;
		[SerializeField]
		private Text _txtLives;

		protected AppModel Model;

		public override void OnStart (object[] args)
		{
			base.OnStart (args);
			Model = AppModel.Instance;
			Model.AddObserver( this );
			OnObjectChanged( Model );
		}

		protected override void OnReleaseResources ()
		{
			base.OnReleaseResources ();
			Model.RemoveObserver( this );
		}

		#region IObserver implementation

		public virtual void OnObjectChanged (Observable observable)
		{
			if( _txtCoins != null )
			{
				_txtCoins.text = string.Format( kTextCoins, Model.Player.Coins );
			}
			if( _txtLives != null )
			{
				_txtLives.text = string.Format( kTextLives, Model.Player.Lives );
			}
		}

		#endregion
	}
}