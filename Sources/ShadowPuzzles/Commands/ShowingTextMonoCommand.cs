﻿using MeliorGames.Core;
using UnityEngine.UI;

namespace ShadowPuzzles.Commands
{
	public class ShowingTextMonoCommand : StateCommand 
	{
		public class Params
		{
			public readonly Text[] ShowingTxt;
			public readonly float Delay;

			public Params( Text[] text, float delay = 0 )
			{
				ShowingTxt = text;
				Delay = delay;
			}

			public Params( Text text, float delay = 0 )
			{
				ShowingTxt = new Text[]{ text };
				Delay = delay;
			}
		}
		private Params _txt;
		private string[] _message;

		private int _countLetter;
		private int _countTxt;

		protected override void OnStart (object[] args)
		{
			_txt = args[0] as Params;
			_message = new string[ _txt.ShowingTxt.Length ];
			for( int i = 0; i < _txt.ShowingTxt.Length; i++ )
			{
				_message[i] = _txt.ShowingTxt[i].text;
				_txt.ShowingTxt[i].text = string.Empty;
			}
			_countTxt = 0;
			_countLetter = 0;
			ScheduleUpdate( _txt.Delay, false );
		}

		protected override void OnScheduledUpdate ()
		{
			base.OnScheduledUpdate ();
			if( _txt.ShowingTxt.Length <= _countTxt || _txt.ShowingTxt[ _countTxt ] == null )
			{
				UnscheduleUpdate();
				FinishCommand();
			}
			else
			{
				if( _countTxt == 0 && _countLetter == 0 )
				{
					ScheduleUpdate( 0, true );
				}
				if( _countTxt == _txt.ShowingTxt.Length )
				{
					UnscheduleUpdate();
					FinishCommand();
				}
				else
				{
					if( _countLetter == _message[ _countTxt ].Length )
					{
						_countLetter = 0;
						_countTxt++;
					}
					else
					{
						_txt.ShowingTxt[ _countTxt ].text += _message[ _countTxt ][ _countLetter ];
						_countLetter++;
					}
				}
			}
		}
	}
}