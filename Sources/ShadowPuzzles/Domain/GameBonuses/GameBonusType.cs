//  TipType.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	11/23/2015 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
namespace ShadowPuzzles.Domain.GameBonuses
{
	public enum GameBonusType
	{
		ResolveLevel,
		SwapElements,
		SelectCrouseType,
		ShowPicture,
		AditionalSteps,
		AditionalLives
	}
}