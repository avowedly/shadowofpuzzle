//  LevelStatisticInfo.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	12/02/2015 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System.Runtime.Serialization;
using MeliorGames.Domain;


namespace ShadowPuzzles.Domain
{
	public struct LocationParametrs
	{
		public int NumberInLocation;
		public GameType GameType;
		
		public LocationParametrs( int levelLocationNumber, GameType gameType )
		{
			NumberInLocation = levelLocationNumber;
			GameType = gameType;
		}
	}

	public sealed class LevelStatisticInfo : DomainObject
	{
		public readonly int Number;
		public readonly LocationParametrs Parametrs;
		public bool IsLevelUnlocked;
		public int LevelStars;
		public int Score;

		public LevelStatisticInfo(SerializationInfo info, StreamingContext context):base(info,context)
		{
		}

		public LevelStatisticInfo( int levelNumber, LocationParametrs parametrs )
		{
			Number = levelNumber;
			Parametrs = parametrs;
			IsLevelUnlocked = false;
			LevelStars = 0;
			Score = 0;
		}
	}
}