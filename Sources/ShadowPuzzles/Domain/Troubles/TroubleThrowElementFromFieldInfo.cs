//  TroubleThrowElementFromFiledInfo.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	11/04/2015 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
namespace ShadowPuzzles.Domain.Troubles
{
	public sealed class TroubleThrowElementFromFieldInfo : TroubleInfo
	{
		public int TargetElementIndex = 0;
		public PositionInField TargetThrowPosition = new PositionInField( 0, 0 );
		
		public override TroubleType Type 
		{
			get
			{
				return TroubleType.Move_Element_Outside_Field;
			}
		}
		
		public override string Description 
		{
			get
			{
				return base.Description + string.Format("move element outside field");
			}
		}
		
		public override TroubleInfo GetClone ()
		{
			TroubleThrowElementFromFieldInfo result = new TroubleThrowElementFromFieldInfo();
			result.TargetElementIndex = this.TargetElementIndex;
			result.TargetThrowPosition = this.TargetThrowPosition;
			
			return result;
		}
	}
}