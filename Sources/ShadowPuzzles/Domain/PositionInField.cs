//  ElementPosition.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	10/22/2015 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;

namespace ShadowPuzzles.Domain
{
	[Serializable]
	public struct PositionInField
	{
		public static PositionInField OutsidePos
		{
			get
			{
				return new PositionInField( -1, -1 );
			}
		}

		public int X;
		public int Y;

		public PositionInField( int x, int y )
		{
			this.X = x;
			this.Y = y;
		}

		public override bool Equals (object obj)
		{
			return base.Equals (obj);
		}

		public override int GetHashCode ()
		{
			return base.GetHashCode ();
		}

		public static bool operator == ( PositionInField posOne, PositionInField posTwo ) 
		{
			return posOne.X == posTwo.X && posOne.Y == posTwo.Y;
		}

		public static bool operator != ( PositionInField posOne, PositionInField posTwo ) 
		{
			return posOne.X != posTwo.X || posOne.Y != posTwo.Y;
		}

		public static PositionInField operator + ( PositionInField posOne, PositionInField posTwo ) 
		{
			return new PositionInField( posOne.X + posTwo.X, posOne.Y + posTwo.Y );
		}

		public static PositionInField operator - ( PositionInField posOne, PositionInField posTwo ) 
		{
			return new PositionInField( posOne.X - posTwo.X, posOne.Y - posTwo.Y );
		}

		public override string ToString ()
		{
			return string.Format ("X:{0}, Y:{1}", X, Y);
		}
	}
}