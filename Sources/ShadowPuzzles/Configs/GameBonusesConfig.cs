//  TipsConfig.cs
//  Author:
//       Oleksandr Halinskyi <oleksandr.galynskyi@meliorgames.com>
// Company:
//        MeliorGames
// Date:
//  	11/23/2015 
// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using MeliorGames.Data;
using MeliorGames.Data.Serialization;
using System.Collections.Generic;
using ShadowPuzzles.Domain.GameBonuses;
using ShadowPuzzles.Domain;


namespace ShadowPuzzles.Configs
{
	public sealed class GameBonusesConfig : Config<GameBonusesConfig, JSonSerializationPolicy>
	{
		public const string FILE_NAME = "Bonuses";

		private static Dictionary<GameBonusType, string> _mappingGameBonusToSpriteInGame;
		private static Dictionary<GameBonusType, string> _mappingGameBonusToSpriteNameInMap;

		static GameBonusesConfig()
		{
			_mappingGameBonusToSpriteInGame = new Dictionary<GameBonusType, string>();
			_mappingGameBonusToSpriteInGame.Add( GameBonusType.AditionalLives, BonusesAtlas.Choose_Cruser_Small );
			_mappingGameBonusToSpriteInGame.Add( GameBonusType.AditionalSteps, BonusesAtlas.Choose_Cruser_Small );
			_mappingGameBonusToSpriteInGame.Add( GameBonusType.ResolveLevel, BonusesAtlas.Resolve_Small );
			_mappingGameBonusToSpriteInGame.Add( GameBonusType.SelectCrouseType, BonusesAtlas.Choose_Cruser_Small );
			_mappingGameBonusToSpriteInGame.Add( GameBonusType.ShowPicture, BonusesAtlas.FinalPicture_Small );
			_mappingGameBonusToSpriteInGame.Add( GameBonusType.SwapElements, BonusesAtlas.Switch_Small );

			_mappingGameBonusToSpriteNameInMap = new Dictionary<GameBonusType, string>();
			_mappingGameBonusToSpriteNameInMap.Add( GameBonusType.AditionalLives, BonusesAtlas.IconBig_Switch );
			_mappingGameBonusToSpriteNameInMap.Add( GameBonusType.AditionalSteps, BonusesAtlas.IconBig_Switch );
			_mappingGameBonusToSpriteNameInMap.Add( GameBonusType.ResolveLevel, BonusesAtlas.IconBig_Resolve );
			_mappingGameBonusToSpriteNameInMap.Add( GameBonusType.SelectCrouseType, BonusesAtlas.IconBig_Choose_Cruser );
			_mappingGameBonusToSpriteNameInMap.Add( GameBonusType.ShowPicture, BonusesAtlas.IconBig_Tube_FinalPicture );
			_mappingGameBonusToSpriteNameInMap.Add( GameBonusType.SwapElements, BonusesAtlas.IconBig_Switch );
		}

		private readonly Dictionary<GameBonusType, GameBonusInfo> _mappingGameBonuses;
		private readonly Dictionary<GameType, GameBonusType[] > _mappingGameTypeToBonuses;

		public readonly int AditionalStepsPersent = 20;
		public readonly int MinAditionalSteps = 2;
		public readonly int AditionalLivesBonus = 5;

		public GameBonusesConfig ():base(FILE_NAME)
		{
			_mappingGameBonuses = new Dictionary<GameBonusType, GameBonusInfo>();
			string description = "resolving level";
			_mappingGameBonuses.Add( GameBonusType.ResolveLevel,
			                        new GameBonusInfo( GameBonusType.ResolveLevel, 15, 0, description ) );
			description = string.Format( "+{0}% aditional steps", AditionalStepsPersent );
			_mappingGameBonuses.Add( GameBonusType.AditionalSteps, 
			                        new GameBonusInfo( GameBonusType.AditionalSteps, 9, 0, description ) );
			description = "Swap any puzzles element";
			_mappingGameBonuses.Add( GameBonusType.SwapElements,
			                        new GameBonusInfo( GameBonusType.SwapElements, 9, 0, description ) );
			description = "Select another crouser type";
			_mappingGameBonuses.Add( GameBonusType.SelectCrouseType, 
			                        new GameBonusInfo( GameBonusType.SelectCrouseType, 9, 0, description ) );
			description = "Show puzzle final picture";
			_mappingGameBonuses.Add( GameBonusType.ShowPicture, 
			                        new GameBonusInfo( GameBonusType.ShowPicture, 9, 0, description ) );
			description = string.Format( "+{0} lives", AditionalLivesBonus );
			_mappingGameBonuses.Add( GameBonusType.AditionalLives, 
			                        new GameBonusInfo( GameBonusType.AditionalLives, 12, 0, description ) );

			_mappingGameTypeToBonuses = new Dictionary<GameType, GameBonusType[]>();
			_mappingGameTypeToBonuses.Add( GameType.Crouser, new GameBonusType[]{ 
				GameBonusType.ResolveLevel, GameBonusType.SwapElements,GameBonusType.SelectCrouseType } );
			_mappingGameTypeToBonuses.Add( GameType.Pipe, new GameBonusType[]{ 
				GameBonusType.ResolveLevel, GameBonusType.SwapElements,GameBonusType.ShowPicture } );
			_mappingGameTypeToBonuses.Add( GameType.Mozaik, new GameBonusType[]{ 
				GameBonusType.ResolveLevel, GameBonusType.SwapElements } );
		}

		public GameBonusInfo GetInfo( GameBonusType bonusType )
		{
			return _mappingGameBonuses[bonusType];
		}

		public GameBonusType[] GetBonusesForGame( GameType gameType )
		{
			return _mappingGameTypeToBonuses[ gameType ];
		}

		public string GetSpriteForBonusInGame( GameBonusType bonus )
		{
			return _mappingGameBonusToSpriteInGame[ bonus ];
		}

		public string GetSpriteForBonusInMap( GameBonusType bonus )
		{
			return _mappingGameBonusToSpriteNameInMap[ bonus ];
		}
	}
}