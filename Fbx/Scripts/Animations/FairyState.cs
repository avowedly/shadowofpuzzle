﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace animationsyur
{
	public enum EFairyState
	{
		Point,
		Victory,
		Sad,
		SadTreas,
		Happy,
		VeryHappy,
		Idle
	}

	public enum EShadowState
	{
		Angry,
		Action,
		Happy,
		Idle
	}

	public class FairyState : MonoBehaviour
	{
		public AnimationClip AnimationClip;
		public string Arms;
		public string Face;
		public EFairyState StateType;
		public EShadowState StateShadowType;
		public List<SoundEffect> SoundEffects;
		public List<ParticleSystem> LstParticles;
	}
}