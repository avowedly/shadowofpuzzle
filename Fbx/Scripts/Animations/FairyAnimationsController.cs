﻿using UnityEngine;
using System.Collections;

namespace animationsyur
{
	public class FairyAnimationsController : MonoBehaviour 
	{
		[SerializeField]
		private AnimationItemsManager _arms;
		[SerializeField]
		private AnimationItemsManager _faces;
		[SerializeField]
		private FairyState[] _fairyStates;
		[SerializeField]
		private Vector2 _screenPos;
		[SerializeField]
		private Animation _fairyAnimation;

		void OnGUI()
		{
			for (int animationNameN = 0; animationNameN < _fairyStates.Length; animationNameN++)
			{
				if( GUI.Button(new Rect( _screenPos.x,_screenPos.y+animationNameN*30,180,30), _fairyStates[ animationNameN ].gameObject.name ))
				{
				
					_arms.SetItem(  _fairyStates[ animationNameN ].Arms );
					_faces.SetItem(_fairyStates[ animationNameN ].Face );

					if( _fairyAnimation.clip.name != _fairyStates[ animationNameN ].AnimationClip.name )
					{
						_fairyAnimation.clip =  _fairyStates[ animationNameN ].AnimationClip;
						_fairyAnimation.Play( _fairyStates[ animationNameN ].AnimationClip.name);
					}
				}
			}
		}
	}
}
