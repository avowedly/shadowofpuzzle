using UnityEngine;
using System.Collections;

public class ScenesMenu : MonoBehaviour 
{
	
//	static string[] sceneNames = new string[] { "Mummy","OrangeDino","VoodooDoll","CyanGhost","GumBoss","BigGum","MiniGum","UniversalZombie","Fish","ZombieBoss","Explosion","SeaSnake","Bat","Robot","Boletus","Boletus1","Ent","Menu","MenuFire","MenuFire1","Death","ZombieCanonMan","EvilMushroom"};
	
	static string[] sceneNames = new string[] {"Shadow","Lake","Sea","Sea","Fairy","Fish","Elephant","Ship2"};
	float buttonSpacing = 30.1f;
	int buttonOffset = 5;
	static float verticalScroll = 0;
	bool useScroll= false;
	//float buttonVerticalSize = 30;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
	
	void OnGUI ()
	{
//		float horizRatio = Screen.width / 960;
		//float vertRatio = Screen.height / 640;
	//	 GUI.matrix = Matrix4x4.TRS (Vector3.zero, Quaternion.identity,new Vector3(horizRatio,vertRatio , 1));
		
		if( useScroll)
		{
			float maxScroll= sceneNames.Length*25;
	       verticalScroll = GUI.VerticalScrollbar(new Rect(120, 10, 300, 570), verticalScroll, 1.0F, 0,maxScroll);
		}
		
		for(int sceneNameN = 0; sceneNameN < 	sceneNames.Length ; sceneNameN++)
		{
			if( sceneNames[ sceneNameN ].Contains("{"))
				GUI.color = Color.green;
			else 
				GUI.color = Color.white;
			if(sceneNames[ sceneNameN ]!="")
				if(GUI.Button(new Rect(750+buttonOffset, -verticalScroll + buttonOffset + (buttonSpacing + buttonOffset)* sceneNameN, 150, buttonSpacing), sceneNames[ sceneNameN ]))
				{
					if(!( sceneNames[ sceneNameN ].Contains("{")))
						Application.LoadLevel( sceneNames[ sceneNameN ] );
				}
			
		}
	}
}
