﻿using UnityEngine;
using System.Collections;

public class EnableRendererByZ : MonoBehaviour
{

	[SerializeField]
	private float _minZ;
	[SerializeField]
	private Renderer _renderer;
	[SerializeField]
	private bool _debug = false;
	[SerializeField]
	private bool _useRendersPos = false;
	

	// Update is called once per frame
	void LateUpdate ()
	{
		if(_debug)
			Debug.Log (_renderer.transform.localPosition.z);
		if(_useRendersPos)
			_renderer.enabled =  _renderer.transform.localPosition.z > _minZ;
		else
			_renderer.enabled =  transform.localPosition.z > _minZ;
	}
}
