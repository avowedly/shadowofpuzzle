﻿using UnityEngine;
using System.Collections;

public class EnableParticleByZ : MonoBehaviour
{
	[SerializeField]
	private float _minZ;
	[SerializeField]
	private ParticleSystem _particleSystem;

	private void Start()
	{
		_particleSystem = gameObject.GetComponentInChildren<ParticleSystem>();
	}
	
	private void Update ()
	{
		if( transform.localPosition.z > _minZ )
		{
			if( !_particleSystem.isPlaying )
			{
				_particleSystem.Play();
			}
		}
		else
		{
			if( _particleSystem.isPlaying )
			{
				_particleSystem.Stop();
			}
		}
	}
}
