﻿using UnityEngine;
using System.Collections;
namespace BlinksImageSequence
{
	public class BlinksImageSequenceController : MonoBehaviour 
	{
		[SerializeField]
		private BlinkImageSequenceEye[] _blinks;
		[SerializeField]
		private float _maxInterval = 5;
		private float _currentInterval = 5;
		private float _timer = 0;

		// Use this for initialization
		void Start () 
		{
			Reset ();
		}
		
		// Update is called once per frame
		void Update () 
		{
			_timer += Time.deltaTime;
			if (_timer > _currentInterval)
			{
				Blink();
				Reset();
			}
		}

		void Reset()
		{
			_timer = 0;
			_currentInterval = Random.Range ((int)(_maxInterval * 0.5f), _maxInterval);

		}

		void Blink()
		{
			for(int blinkN = 0; blinkN < _blinks.Length ; blinkN++)
			{
				_blinks [ blinkN].Play();
			}
		}
	}
}
