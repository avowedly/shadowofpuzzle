﻿using UnityEngine;
using System.Collections;
using MeliorGames.UI;

namespace animationsyur
{
	public class ShadowAnimationsController : MGComponent 
	{
		[SerializeField]
		private AnimationItemsManager _arms;
		[SerializeField]
		private FairyState[] _fairyStates;
		[SerializeField]
		private Vector2 _screenPos;
		[SerializeField]
		private Animation _fairyAnimation;

		protected override void OnUpdate ()
		{
			base.OnUpdate ();
			if (_fairyAnimation.isPlaying == false)
			{
				PlayAnimation (0);
			}
		}

		void OnGUI()
		{
			for (int animationNameN = 0; animationNameN < _fairyStates.Length; animationNameN++)
			{

				if( GUI.Button(new Rect( _screenPos.x,_screenPos.y+animationNameN*30,180,30), _fairyStates[ animationNameN ].gameObject.name ))
				{
				
					_arms.SetItem(  _fairyStates[ animationNameN ].Arms );
					if( _fairyAnimation.clip.name != _fairyStates[ animationNameN ].AnimationClip.name || _fairyAnimation.isPlaying == false )
					{
						_fairyAnimation.clip =  _fairyStates[ animationNameN ].AnimationClip;
						_fairyAnimation.Play( _fairyStates[ animationNameN ].AnimationClip.name);
					}
				}
			}
		}

		private void PlayAnimation(int index)
		{
			_arms.SetItem(  _fairyStates[index].Arms );
			_fairyAnimation.clip =  _fairyStates[ index ].AnimationClip;
			_fairyAnimation.Play( _fairyStates[ index ].AnimationClip.name);
		}
	}
}
