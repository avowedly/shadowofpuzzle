using UnityEngine;
using System.Collections;

namespace animationsyur
{

	public class AnimationItem : MonoBehaviour 
	{

		[SerializeField]
		private GameObject[] _gameObjects;
		[SerializeField]
		private bool _onByDefault = false;
		private bool _active = false;
		void Start()
		{

			SetActive (_onByDefault);
		}
		

		public void SetActive(bool isActive)
		{
			for (int objectN = 0; objectN <  _gameObjects.Length; objectN++)
				_gameObjects [objectN].SetActive (isActive);
			_active = isActive;

		}
		
		public bool IsActive()
		{
			return _active;
			
		}
	}



}
