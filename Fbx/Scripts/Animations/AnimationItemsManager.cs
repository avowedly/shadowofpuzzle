﻿using UnityEngine;
using System.Collections;

namespace animationsyur
{
	public class AnimationItemsManager : MonoBehaviour 
	{
		[SerializeField]
		private Vector2 _screenPos;
		[SerializeField]
		private AnimationItem[] _items;

		public void SetItem( string itemName)
		{
			for(int itemN = 0; itemN < _items.Length; itemN++)
				_items[itemN].SetActive(false);
			for(int itemN = 0; itemN < _items.Length; itemN++)
			{
				if(itemName.Contains( _items[itemN].gameObject.name))
					_items[itemN].SetActive(true);

			}
		}
	}
}
