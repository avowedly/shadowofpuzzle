﻿using UnityEngine;
using System.Collections;

namespace FoodFactoryAnimations
{
	public class AnimatedUVs : MonoBehaviour
	{
		public int materialIndex = 0;
		public Vector2 uvAnimationRate = new Vector2( 1.0f, 0.0f );
		public string textureName = "_MainTex";
		[SerializeField]
		private Material _material;
		Vector2 uvOffset = Vector2.zero;
		[SerializeField]
		private Renderer _renderer;

		void LateUpdate()
		{
			uvOffset += ( uvAnimationRate * Time.deltaTime );
		//	if( renderer.enabled )
		//	{
				_material.SetTextureOffset( textureName, uvOffset );
		//	}
		}
	}
}