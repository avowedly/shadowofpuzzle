﻿using UnityEngine;
using System.Collections;

public class SetSortingOrder : MonoBehaviour
{
	[SerializeField]
	private int _setSortingOrderLayer = 0;
	// Use this for initialization
	void Start ()
	{
		if(renderer!=null)
			renderer.sortingOrder = _setSortingOrderLayer;
	}

}
