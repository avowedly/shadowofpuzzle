using UnityEngine;
using System.Collections;

public class AnimationButtons : MonoBehaviour 
{

	public Animation characterAnimation;
	public string[] hiddenAnimsList;
	public string[] animsWithParticles;
	public bool revertToIdleAfterAnim = false;
	public string idleAnimName =  "Idle0";

	void Start()
	{


	}
	
	void Update()
	{
		if(	 !characterAnimation.isPlaying && revertToIdleAfterAnim)
				characterAnimation.Play( idleAnimName );
				
	}
	
	public bool useHiddenAnimsList = false;
	public int horizontalButtonOffset = 0;
	int maxButtonsColumn = 24;
	
	void OnGUI()
	{
		int i=0;
		int currentAnimationsColumnN = 0;
		int horizontalOffset =0;
		foreach(AnimationState state in characterAnimation)
		{
		
			if( !IsHidden( state.name))
			{
				
				i++;
				  if( GUI.Button(new Rect(horizontalButtonOffset,currentAnimationsColumnN*20,190,20),state.name))
					{
						characterAnimation.Stop();
						characterAnimation.clip = state.clip;
					
						characterAnimation.Play();
						StartParticles( state.name);
					}
					
					currentAnimationsColumnN++;
				
				if( currentAnimationsColumnN > maxButtonsColumn)
				{
					horizontalOffset +=	 horizontalButtonOffset;
					currentAnimationsColumnN = 0;
				}
			}
			
		}
		
	
		
	}
	
	bool IsHidden(string animName)
	{
			bool hidden = false;
			for(int i = 0; i <	hiddenAnimsList.Length; i++)
				if ( animName == hiddenAnimsList[i])
						hidden = true;
		return  hidden;
		
	}
	

	void StartParticles(string animName)
	{
		if(animsWithParticles != null)
			for (int animN = 0; animN < animsWithParticles.Length; animN++)
				if(animsWithParticles[animN] == animName)
				{
					GameObject particleGameObjectPrefab = Resources.Load(characterAnimation.name + "/"+animName) as GameObject;
					GameObject.Instantiate(particleGameObjectPrefab);
					
				}
	}

}
