using UnityEngine;
using System.Collections;
using Sources.ShadowPuzzles.Domain;

namespace animationsyur
{
	public class FairyCostumeSettings : MonoBehaviour 
	{
		public Material CostumeMaterial;
		public Renderer[] SpecialRenderers;
		public FairyCostume CostumeType;
	}
}