﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour
{
	[SerializeField]
	private Vector3 _vector;
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		transform.Rotate (_vector * Time.deltaTime,Space.Self);
	}
}
