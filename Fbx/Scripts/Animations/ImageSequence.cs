﻿using UnityEngine;
using System.Collections;

namespace BlinksImageSequence
{
	public class ImageSequence : MonoBehaviour 
	{
		[SerializeField]
		private Texture[] _eyeAnimationSequence;
		private int _index = 0;
		[SerializeField]
		private float _timePerFrame = 0.3f;
		private float _timer = 0;
		// Use this for initialization
		void Start () 
		{
			Play ();
		}
		
		// Update is called once per frame
		void Update () 
		{
			_timer += Time.deltaTime;
			if (_timer > _timePerFrame) 
			{
				_index++;
				if( _index > _eyeAnimationSequence.Length-1)
				{
					_index = 0;
				}
				else
				{
					_timer = 0;
					SetFrame(_index);
				}
			}
		}

		public void Play()
		{
			_index = 0;
			enabled = true;
			renderer.enabled = true;
			SetFrame (0);
		}

		public void Stop()
		{
			enabled = false;
			renderer.enabled = false;
			_index = 0;
		}

		void SetFrame(int index)
		{
			renderer.material.mainTexture = _eyeAnimationSequence [index];
		}
	}
}
