using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sources.ShadowPuzzles.Domain;

namespace animationsyur
{
	public class FairyCostumes : MonoBehaviour
	{
		[SerializeField]
		private Renderer[] _specialRenderers;
		[SerializeField]
		private Renderer[] _renderers;
		[SerializeField]
		private List<FairyCostumeSettings> _costumes;

		public void SelectCostume( FairyCostume costumeType )
		{
			FairyCostumeSettings currentCostume = _costumes.Find( costume => costume.CostumeType == costumeType );
			for( int rendererN = 0; rendererN < _renderers.Length; rendererN++)
			{
				_renderers[rendererN].material = currentCostume.CostumeMaterial;
			}
			
			for (int specialRendererN = 0; specialRendererN < _specialRenderers.Length; specialRendererN++)
			{
				_specialRenderers [specialRendererN].enabled = false;
			}
			
			for (int specialRendererN = 0; specialRendererN < currentCostume.SpecialRenderers.Length; specialRendererN++)
			{
				currentCostume.SpecialRenderers[specialRendererN].enabled = true;
			}
		}
	}
}