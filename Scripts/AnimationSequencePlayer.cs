﻿using UnityEngine;
using System.Collections;

public class AnimationSequencePlayer : MonoBehaviour 
{
	[SerializeField]
	private string[] _names;
	private int _index = 0;
	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if( animation.isPlaying == false)
		{
			_index++;
			if( _index >= _names.Length)
				_index = 0;
			animation.Play( _names[_index]);
		}
	}
}
